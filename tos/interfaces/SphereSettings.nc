/* provided by DSDV_SoI_Settings module, used by DSDV_SoI_Mettric &
   SettingsHandler */

includes WSN;

interface SphereSettings {
   event void enableSoI(bool ToF);
   event void enableAdjuvantNode(bool ToF);
   event wsnAddr getPrimarySphereID();
   command uint16_t getAdjuvantValue();
   command void setDefault(bool isSoIEnabled, bool amAdjuvantNode, uint16_t valueFunc);
}

/**Adapted code from RFC 3174 for NesC*/

includes sha1;

interface CRIPTO{

	command int SHA1Reset(  SHA1Context * context);
	command int SHA1Input(  SHA1Context * context, const uint8_t *message_array ,uint32_t length);
	command int SHA1Result( SHA1Context *context , uint8_t Message_Digest[SHA1HashSize]);
	command void RSAGenerateKeys(SAKM_Public_Key *pk, uint32_t a, uint32_t b);
	command void RSA_Encription(uint32_t pk[],uint32_t message[],uint32_t message_size,uint32_t signature_field[]);
	command uint32_t hmac_sha(uint8_t k[], /* secret key */ uint8_t lk, /* length of the key in bytes */ uint8_t d[], /* data */ uint8_t ld, /* length of data in bytes */ uint8_t out[]);
	
	}


/**Definition of message types value*/



//Definition of hash F sign values
#define RESERVED 0
#define MD2 1
#define MD5	2
#define SHA_1 3
#define SHA_256 4
#define SHA_384 5
#define SHA_512 6

//Definition of signature types values
#define RSA 1
#define DSA 2
#define ELGAMAL 3

//Default hash and signature method definitions
uint8_t DEFAULT_HASH_FUNCTION=SHA_1;
uint8_t DEFAULT_SIGN_METHOD=RSA;



 

typedef struct {
	uint32_t reserved_and_length;
	uint32_t value[1];
} SAKM_Public_Key;

typedef struct{
	uint8_t hash_f_sign;
	uint16_t reserved;
	uint8_t length;
	uint32_t value[1];

}SAKM_Signature;

typedef struct{
	uint32_t exp_reserved_and_length;
	uint32_t modulus[1];
}SAKM_RSA;

typedef struct{
	uint32_t pqg_reserved_and_lenth;
	uint32_t pub_key_y;
}SAKM_DSA;

typedef struct{
	uint32_t pg_reserved_and_length;
	uint32_t pub_key_y;
}SAKM_ELGAMAL;





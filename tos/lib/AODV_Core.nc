/* 
 * AODV_core.c - Core AODV module
 * Author:	
 * 		Intel Corporation
 * Date:	$Date: 2004/12/31 20:08:27 $
 *
 * Copyright (c) 2003 Intel Corporation
 */


#define AODV_RTABLE_SIZE   7
#define AODV_RQCACHE_SIZE  7
#define AODV_RPCACHE_SIZE  7

#define AODVR_NUM_TRIES  4

#define AODV_MAX_RAND    4

// this will bethe max number of hops
#define AODV_MAX_METRIC  10  
#define D_FLAG 1
#define G_FLAG 2


#if PLATFORM_PC
#define AODV_CORE_DEBUG  1
#endif

#define RREQ_RANDOMIZE 1

#define RREQ_SSE 0
#define RREQ_DSE 1
 
module AODV_Core {
    provides {
	interface StdControl as Control;
	interface RouteLookup;
	interface ReactiveRouter;
	interface RouteError;
    }
    uses {
	interface Random;
	// Route Request interfaces
	interface SendMsg as SendRreq;
	interface Payload as RreqPayload;
	interface ReceiveMsg as ReceiveRreq;
	// Route Reply interfaces
	interface SendMsg as SendRreply;
	interface Payload as RreplyPayload;
	interface ReceiveMsg as ReceiveRreply;
	// Route repair interfaces
	interface SendMsg as SendRerr;
	interface Payload as RerrPayload;
	interface ReceiveMsg as ReceiveRerr;
    //Duplicated address interface
	interface SendMsg as SendDadd;
	interface Payload as DaddPayload;
	interface ReceiveMsg as ReceiveDadd;
	//New address interface
	interface SendMsg as SendNadd;
	interface Payload as NaddPayload;
	interface ReceiveMsg as ReceiveNadd;
	//New address ack interface
	interface SendMsg as SendNadd_ack;
	interface Payload as Nadd_ackPayload;
	interface ReceiveMsg as ReceiveNadd_ack;
	
	
	interface Timer;
	interface SingleHopMsg; // to decode single hop headers
	//interface StdControl as MetricControl;
	interface StdControl as ForwardingControl;
	interface StdControl as RadioControl;
	interface Leds;
	interface CRIPTO;
    }
}

implementation {
    
    // Local allocated message buffers
    TOS_Msg msgBuf1, msgBuf2, msgBuf3, msgBuf4, msgBuf5, msgBuf6, msgBuf7;
    TOS_MsgPtr rreqMsg;
    TOS_MsgPtr rReplyMsg;
	TOS_MsgPtr rReplyMsg2;
    TOS_MsgPtr rErrMsg;
    TOS_MsgPtr daddMsg;
    TOS_MsgPtr naddMsg;
    TOS_MsgPtr nadd_ackMsg;
	
    uint8_t rreqTaskPending;
    uint8_t rreplyTaskPending;
	uint8_t rreplyTaskPending2;
    uint8_t rerrTaskPending;
    uint8_t daddTaskPending;
    uint8_t naddTaskPending;
    uint8_t nadd_ackTaskPending;
#if RREQ_RANDOMIZE
    uint16_t rreqRandomize;
#endif
    bool sendPending;
    int rreqNumTries;
    int rreplyNumTries;
	int rreplyNumTries2;
    int rerrNumTries;
    int daddNumTries;
    int naddNumTries;
    int nadd_ackNumTries;

	uint16_t DEFAULT_FLAGS; 
    wsnAddr rreqDest;
    wsnAddr rreqSrc;
    wsnAddr rReplyDest;
    wsnAddr rReplySrc;
    wsnAddr rErrDest;
    wsnAddr rErrSrc;
	wsnAddr daddDest;
	wsnAddr daddSrc;
	wsnAddr naddSrc;
	wsnAddr nadd_old_ip;
	wsnAddr nadd_new_ip;
	wsnAddr nadd_ackDest;
    uint16_t seq;
    uint16_t rreqID;
	uint8_t num_rrep;
	SAODV_Rreq_Single_Signature_MsgPtr rreqSingleMsg;
	SAODV_Rreq_Double_Signature_MsgPtr rreqDoubleMsg;
    SAODV_Rreq_Single_Signature_Extension_Msg fwdRreq;
	SAODV_Rreq_Double_Signature_Extension_Msg fwdRreqDouble;
    SAODV_Rreply_Single_Signature_Extension_Msg fwdRreply;
    SAODV_Rreply_Double_Signature_Extension_Msg fwdRreplyDouble;
    SAODV_Rreply_Double_Signature_Extension_Msg fwdRreplyDouble2;
	SAKM_Nadd_Msg forwardNadd;

	uint32_t res[5];
	uint32_t pk_ip_duplicated[2];
 	SAKM_Public_Key pk;
	uint8_t DEFAULT_RREQ_EXTENSION;
	uint8_t position;
    //Route table entry. Note: move route table processing into a separate component in next release
    AODV_Route_Table routeTable[AODV_RTABLE_SIZE];
    //Reverse route cache. Note: move cache processing into a separate component in next release
    AODV_Route_Cache rreqCache[AODV_RQCACHE_SIZE];
    // Forward route cache. 
    //  AODV_Route_Table rReplyCache[AODV_RPCACHE_SIZE];
    //
    //  Purpose: Initilaize the AODV module
    //  Returns: Always 1 on success
    	
	void arraychunkcpy(uint32_t s[], uint32_t t[], uint32_t size,uint32_t place){
		int i;

		for(i=0;i<size;i++){
			s[place+i] = t[i];
		}
		
	}
	
	void calculateHash(uint8_t loops,uint32_t hash[],uint32_t size, uint8_t hash_algorithm){
		uint64_t i;
		uint8_t blocks[200];//como minimo 4 veces el tama�o de size
		SHA1Context context;
		int lon;
		
		uint8_t aux[20];
		
		lon=0;
		i=0;
		
		aux[0]=0;
		aux[1]=0;
		aux[2]=0;
		aux[3]=0;
		aux[4]=0;
		
		for(i=0;i<size;i++){
			blocks[(i*4)]=hash[i]>>24;
			blocks[(i*4)+1]=hash[i]>>16 & 0x0FF;
			blocks[(i*4)+2]=hash[i]>>8 & 0x0ff;
			blocks[(i*4)+3]=hash[i] & 0x0FF;
		}
				
		lon=i;
		call CRIPTO.SHA1Reset(&context);
		call CRIPTO.SHA1Input(&context,blocks,(size*4));
		call CRIPTO.SHA1Result(&context,aux);
		
		for(i=0;i<loops;i++){
			call CRIPTO.SHA1Reset(&context);
			call CRIPTO.SHA1Input(&context,aux,lon);
			
			call CRIPTO.SHA1Result(&context,aux);
			lon=20;

		}
		
		if(hash_algorithm==SHA_1){
			lon=20/4;
		}
		for(i=0;i<lon;i++){
			res[i]=aux[(i*4)];
			res[i]=res[i]<<8 | aux[(i*4)+1];
			res[i]=res[i]<<8 |aux[(i*4)+2];
			res[i]=res[i]<<8| aux[(i*4)+3];
		}
		
		
	}
	
	void executeHash(uint32_t hash[], uint8_t hash_algorithm){
		uint64_t i;
		uint8_t blocks[20];//como minimo 4 veces el tama�o de size
		SHA1Context context;
		int lon;
		
		uint8_t aux[20];
		
		lon=0;
		i=0;
		
		for(i=0;i<5;i++){
					
			blocks[(i*4)]=hash[i]>>24;
			blocks[(i*4)+1]=(hash[i]>>16) & 0x0FF;
			blocks[(i*4)+2]=(hash[i]>>8) & 0x0ff;
			blocks[(i*4)+3]=hash[i] & 0x0FF;
		}
			
		call CRIPTO.SHA1Reset(&context);
		call CRIPTO.SHA1Input(&context,blocks,(5*4));
		call CRIPTO.SHA1Result(&context,aux);

		
		for(i=0;i<20;i++){
			hash[i]=aux[(i*4)];
			hash[i]=hash[i]<<8 | aux[(i*4)+1];
			hash[i]=hash[i]<<8 |aux[(i*4)+2];
			hash[i]=hash[i]<<8| aux[(i*4)+3];
		}
	}
	
	void calculateHash2(uint8_t loops,uint32_t hash[],uint32_t size, uint8_t hash_algorithm){
		int i;
		
		
		for(i=0;i<5;i++){
			res[i]=hash[i];
		}
		
		for(i=0; i<loops;i++){
			executeHash(res,hash_algorithm);
			
		
		}
	
	
	}
	
	bool compareSignatures(uint32_t s[], uint32_t t[]){
		int i;
		int stop;
		stop=(s[0] & 0x0ff) +1;
		
		i=0;

		
		if(s[0]!=t[0]){
		
			return FALSE;
		}
		else{
		
			for(i=0;i<stop;i++){
			
				if(s[i]!=t[i]){
					return FALSE;
				}
			}
			return TRUE;		
		}
   	}

	bool compareHash(uint32_t s[], uint32_t t[]){
		int i;
		
		for(i=0;i<5;i++){
				if(s[i]!=t[i]){
				
					return FALSE;
				}
		}
		
		return TRUE;		
		
	}
	
	void keycpy(uint32_t s[]){
		uint32_t i;
		uint32_t stop;
		uint32_t s_index;
		s_index=0;
		s[s_index]=pk.reserved_and_length;
		s_index++;
		stop=pk.reserved_and_length & 0x0ff;
		for(i=0;i<stop;i++){
			s[s_index]= pk.value[i];
			s_index++;
		}
		
	
		
	}
	
	void signMessage(uint32_t message_size,uint32_t message[], uint32_t publickey[],uint32_t hash_function,uint32_t signature_field[]){
		int i;
		for(i=0;i<5;i++){
			res[i]=0;
		}
		calculateHash(1,message,message_size,hash_function);
		signature_field[0]=hash_function<<24 |0;
		call CRIPTO.RSA_Encription(publickey,res,20/4,signature_field);
	}
	
	bool checkRERR(SAODV_Rerr_Signature_Extension_MsgPtr msg){
		uint32_t lon;
		uint32_t signature[5];
		uint32_t message[30];
					
		message[0]=msg->type;
		message[0]=message[0]<<16| msg->n_and_reserved ;
		message[0]=message[0]<<8|msg->destCount;
		message[1]=msg->dest;
		message[2]=msg->destSeq;
		message[3]=msg->dest2;
		message[4]=msg->destSeq2;
		message[5]=msg->type_extension;
		message[5]=message[5]<<24 | 0;
		message[6]=msg->signMethod;
		message[6]=message[6]<<16 | msg->h_and_reserved;
		message[6]=message[6]<<8 |msg->paddLength;
		
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]&0x0ff)+1,7);
		lon=lon+(msg->publicKey[0]&0x0ff)+1;
		arraychunkcpy(message,msg->padding,msg->paddLength,7+lon);
		lon=lon+msg->paddLength;
		
		signMessage(7+lon,message,msg->publicKey,SHA_1,signature);
		lon=lon+(msg->signature[0]& 0x0ff)+1;	
  
    
		if(compareSignatures(msg->signature,signature)==FALSE){
			dbg(DBG_USR1,"Check RERR: Wrong signature\n");
			return FALSE;
		}
		return TRUE;
  
  }
 
	bool checkRREQSingle(SAODV_Rreq_Single_Signature_MsgPtr msg){
		uint32_t lon;
		uint32_t signature[5];
		uint32_t message[30];
				
		calculateHash2(msg->maxHopCount-msg->metric[0],msg->hash,5,msg->hash_function);

	
		if(compareHash(msg->topHash,res)==FALSE){

			dbg(DBG_USR1,"checkRReqSingle: Wrong hop count\n");
			return FALSE;
		}

		message[0]=msg->type;
		message[0]=message[0]<<16 | msg->jrgdu_and_reserved;
		message[0]=message[0]<<8 | 0;
		
		
		message[1]=msg->rreqID;
		message[2]=msg->dest;
		message[3]=msg->destSeq;
		message[4]=msg->src;
		message[5]=msg->srcSeq;
		message[6]=msg->type_extension;
		message[6]=message[6]<<16 |  msg->hash_function;
		message[6]=message[6]<<8| msg->maxHopCount;
		lon=SHA1HashSize/4;
		arraychunkcpy(message,msg->topHash,lon,7);
		message[7+lon]=msg->signMethod;
		message[7+lon]=message[7+lon]<<16 |msg->h_and_reserved;
		message[7+lon]=message[7+lon]<<8 |msg->paddLength;
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]& 0x0ff) +1,8+lon);
		lon=lon+(msg->publicKey[0] & 0x0ff)+1;
		arraychunkcpy(message,msg->padding,(msg->paddLength/32),8+lon);
		lon=lon+(msg->paddLength/32);	
		signMessage(8+lon,message,msg->publicKey,SHA_1,signature);
		
		if(compareSignatures(msg->signature,signature)==FALSE){
			dbg(DBG_USR1,"Check RREQ-SSE: Wrong signature\n");
			return FALSE;
		}
		return TRUE;
   }

	bool checkRREQDouble(SAODV_Rreq_Double_Signature_MsgPtr msg){
		uint32_t lon;
		uint32_t signature[5];
		uint32_t message[50];
				
		calculateHash2(msg->maxHopCount-msg->metric[0],msg->hash,5,msg->hash_function);
		if(compareHash(msg->topHash,res)==FALSE){
			dbg(DBG_USR1,"CheckRREQDouble: Wrong hop count\n");
			return FALSE;
		}

		message[0]=msg->type;
		message[0]=message[0]<<16| msg->jrgdu_and_reserved;
		message[0]=message[0]<<8 | 0;
		message[1]=msg->rreqID;
		message[2]=msg->dest;
		message[3]=msg->destSeq;
		message[4]=msg->src;
		message[5]=msg->srcSeq;
		message[6]=msg->type_extension;
		message[6]=message[6]<<16 |  msg->hash_function;
		message[6]=message[6]<<8 |msg->maxHopCount;
		message[7]=0;
		message[8]=msg->signMethod;
		message[8]=message[8]<<16|0;
		message[8]=message[8]<<8 | msg->paddLength;
		lon=SHA1HashSize/4;
		arraychunkcpy(message,msg->topHash,lon,9);
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]& 0x0ff )+ 1,9+lon);
		lon=lon+((msg->publicKey[0] & 0x0ff)+1);
		arraychunkcpy(message,msg->signatureRREP,(msg->signatureRREP[0]&0x0ff) +1,9+lon);
		lon=lon+(msg->signatureRREP[0]&0x0ff)+1;
		
		signMessage(9+lon,message,msg->publicKey,msg->hash_function,signature);
		
		if(compareSignatures(msg->signature,signature)==FALSE){
			dbg(DBG_USR1,"Check RREQ-DSE: Wrong signature\n");
			return FALSE;
		}

		return TRUE;
   }
	
	bool checkRREPSingle(SAODV_Rreply_Single_Signature_MsgPtr msg){
		uint32_t lon;
		uint32_t signature[5];
		uint32_t message[30];
		calculateHash2(msg->maxHopCount-msg->metric[0],msg->hash,5,msg->hash_function);
		if(compareHash(msg->topHash,res)==FALSE){
			dbg(DBG_USR1,"checkRREPSingle: Wrong hop count\n");
			return FALSE;
		}
		
		message[0]=msg->type;
		message[0]=message[0]<<16 | msg->ra_reserved_and_prefix_size;
		message[0]=message[0]<<8 | 0;
		message[1]=msg->dest;
		message[2]=msg->destSeq;
		message[3]=msg->src;
		message[4]=msg->lifetime;
		message[5]=msg->type_extension;
		message[5]=message[5]<<16 |  msg->hash_function;
		message[5]=message[5]<<8| msg->maxHopCount;
		lon=SHA1HashSize/4;
		arraychunkcpy(message,msg->topHash,lon,6);
		message[6+lon]=msg->signMethod;
		message[6+lon]=message[6+lon]<<16 | msg->H_and_Reserved;
		message[6+lon]=message[6+lon]<<8 | msg->paddLength;
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]& 0x0ff) +1,7+lon);
		lon=lon+(msg->publicKey[0] & 0x0ff)+1;
		arraychunkcpy(message,msg->padding,(msg->paddLength/32),7+lon);
		lon=lon+(msg->paddLength/32);	
		signMessage(7+lon,message,msg->publicKey,SHA_1,signature);
		
		if(compareSignatures(msg->signature,signature)==FALSE){
			dbg(DBG_USR1,"Check RREPLY-SSE: Wrong signature\n");
			return FALSE;
		}
		return TRUE;
	}
    
	bool checkRREPDouble(SAODV_Rreply_Double_Signature_MsgPtr msg){
		uint32_t message[50];
		uint32_t lon;
		uint32_t signature[5];
		
		calculateHash2(msg->maxHopCount-msg->metric[0],msg->hash,5,msg->hash_function);
		
		if(compareHash(msg->topHash,res)==FALSE){
			
			dbg(DBG_USR1,"checkRREPDouble Wrong hop count\n");
			return FALSE;
		}
		
		message[0]=msg->type;
		message[0]=message[0]<<16 | msg->ra_reserved_and_prefix_size;
		message[0]=message[0]<<8 | 0;
		message[1]=msg->dest; //0;Uncomment when TinyOs will support msg_size above 120
		message[2]=msg->destSeq;
		message[3]=msg->oldOriginatorIPAddress;
		message[4]=4000;//msg->oldLifetime; Uncomment when TinyOs will support msg_size above 120
		message[5]=msg->type_extension;
		message[5]=message[5]<<16 |  msg->hash_function;
		message[5]=message[5]<<8| msg->maxHopCount;
		
		lon=SHA1HashSize/4;
		arraychunkcpy(message,msg->topHash,lon,6);
		message[6+lon]=msg->signMethod;
		message[6+lon]=message[6+lon]<<16 | msg->H_and_Reserved;
		message[6+lon]=message[6+lon]<<8 | msg->paddLength;
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]& 0x0ff) +1,7+lon);
		lon=lon+(msg->publicKey[0] & 0x0ff)+1;
		
	
		signMessage(7+lon,message,msg->publicKey,SHA_1,signature);

		if(compareSignatures(msg->signature,signature)==FALSE){
			
			return FALSE;
		}
		
		message[3]=msg->src;
		message[4]=msg->lifetime;
		
		lon=SHA1HashSize/4;
		message[6+lon]=msg->signMethod2;
		message[6+lon]=message[6+lon]<<16 | msg->H_and_Reserved /*msg->H_and_Reserved2 Uncomment when TinyOs supports msg_size> 120*/;
		message[6+lon]=message[6+lon]<<8 | msg->paddLength2;
		
		arraychunkcpy(message,msg->publicKey2,(msg->publicKey2[0]& 0x0ff) +1,7+lon);
		lon=lon+(msg->publicKey2[0] & 0x0ff)+1;
		
		arraychunkcpy(message,msg->padding,(msg->paddLength2/32),7+lon);
		lon=lon+(msg->paddLength2/32);
		
		signMessage(7+lon,message,msg->publicKey2,SHA_1,signature);
		
		if(compareSignatures(msg->signature2,signature)==FALSE){
			
			return FALSE;
		}
		
		
		return TRUE;
	}
	
	bool checkNadd(SAKM_Nadd_MsgPtr msg){
		uint32_t lon;
		uint32_t signature[5];
		uint32_t message[20];
		message[0]=msg->type;
		message[0]=message[0]<<24 | msg->reserved;
		message[1]=msg->signMethod;
		message[1]=message[1]<<16 | msg->h_and_reserved;
		message[1]=message[1]<<8 | msg->paddLength;
		message[2]=msg->old_ip;
		message[3]=msg->new_ip;
		lon=0;	
		arraychunkcpy(message,msg->oldpublicKey,(msg->oldpublicKey[0]&0x0ff)+1,4);
		lon=lon+(msg->oldpublicKey[0]&0x0ff)+1;
		arraychunkcpy(message,msg->padding,msg->paddLength,4+lon); 
		lon=lon+msg->paddLength;
		
		signMessage(4+lon,message,msg->oldpublicKey,SHA_1,signature);
		
		if(compareSignatures(signature,msg->signature_old_key)==FALSE){
			dbg(DBG_USR1,"SAKM: Nadd first signature is wrong\n");
			return FALSE;
		}
		lon=0;
		message[1]=msg->signMethod2;
		message[1]=message[1]<<16 |msg->h_and_reserved2;
		message[1]=message[1]<<8 |msg->paddLength2;
		message[2]=msg->old_ip;
		message[3]=msg->new_ip;
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]&0x0ff)+1,4);
		lon=lon+(msg->publicKey[0]&0x0ff)+1;
		arraychunkcpy(message,msg->padding2,msg->paddLength2,4+lon); 
		lon=lon+msg->paddLength2;
		
				
		signMessage(4+lon,message,msg->publicKey,SHA_1,signature);
		if(compareSignatures(signature,msg->signature_new_key)==FALSE){
		
			return FALSE;
		}
		
		return TRUE;
	}
	
	bool checkNadd_ack(SAKM_Nadd_ack_MsgPtr msg){
		uint32_t message[20];
		uint32_t signature[5];
		uint32_t lon;			

		lon=0;
		message[0]=msg->type;
		message[0]=message[0]<<24 | msg->reserved;
		message[1]=msg->old_ip;
		message[2]=msg->new_ip;
		message[3]=msg->signMethod;
		message[3]=message[3]<<16 |msg->h_and_reserved;
		message[3]=message[3]<<8 |msg->paddLength;
		
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]&0x0ff)+1,4);
		lon=lon+(msg->publicKey[0]&0x0ff)+1;

		
		arraychunkcpy(message,msg->padding,msg->paddLength,4+lon); 
		lon=lon+msg->paddLength;
			
		signMessage(4+lon,message,msg->publicKey,SHA_1,signature);
		
		if(compareSignatures(msg->signature,signature)==FALSE){
			
			return FALSE;
		}
		return TRUE;
    }
	
	
	void generateRREP(wsnAddr addr,uint32_t srcSeq, uint32_t signatureRREP[],uint32_t hash[],uint32_t topHash[],uint32_t padd){
		int aux;
		
		uint32_t message[50];
		int i;
		int lon;
		SAODV_Rreply_Double_Signature_Extension_Msg rreply;
		lon=0;
		i=0;
		rreply.type=AODV_RREP;
		rreply.ra_reserved_and_prefix_size=0;
		rreply.metric[0]=0;
		rreply.dest=TOS_LOCAL_ADDRESS;
		rreply.destSeq=srcSeq;
		rreply.src=addr;
		rreply.lifetime=4000;
		rreply.type_extension=SAODV_RREP_DOUBLE_SIGNATURE_EXTENSION;
		rreply.length=0;
		rreply.hash_function=DEFAULT_HASH_FUNCTION;
		rreply.maxHopCount=DEFAULT_MAX_HOP_COUNT;
		lon=SHA1HashSize/4;
		arraychunkcpy(rreply.hash,hash,SHA1HashSize/4,0);
		arraychunkcpy(rreply.topHash,topHash,SHA1HashSize/4,0);
		rreply.signMethod=DEFAULT_SIGN_METHOD;
		rreply.H_and_Reserved=0;
		rreply.paddLength=(uint8_t) padd;
		for(aux=0;aux<rreply.paddLength;aux++){
				rreply.padding[aux]=0;
		}
		
		keycpy(rreply.publicKey);
		message[0]=rreply.type;
		message[0]=message[0]<<16 |rreply.ra_reserved_and_prefix_size;
		message[0]=message[0]<<8 | 0;
		message[1]=rreply.dest;
		message[2]=rreply.destSeq;
		message[3]=rreply.src;
		message[4]=rreply.lifetime;
		message[5]=rreply.type_extension;
		message[5]=message[5]<<16 |rreply.hash_function;
		message[5]=message[5]<<8|rreply.maxHopCount;
		arraychunkcpy(message,rreply.topHash,5,6);
		lon=5;
		message[6+lon]=rreply.signMethod;
		message[6+lon]=message[6+lon]<<16|rreply.H_and_Reserved;
		message[6+lon]=message[6+lon]<<8|rreply.paddLength;
			
			
		arraychunkcpy(message,rreply.publicKey,(rreply.publicKey[0]&0x0ff)+1,7+lon);
		lon=lon+(rreply.publicKey[0]&0x0ff)+1;
		/*Uncomment when TinyOS will support  message sizes above 120 */
		//arraychunkcpy(message,rreply.padding,(rreply.paddLength/32),7+lon);
		//lon=lon+(rreply.paddLength/32);	
	
		
		signMessage(7+lon,message,rreply.publicKey,SHA_1,signatureRREP);
			
	}	
	
	result_t markEntry(wsnAddr src,uint32_t publicKey[]){
		int i;
		for(i=0; i< AODV_RQCACHE_SIZE; i++){
		    if((rreqCache[i].src == src)&& (compareSignatures(publicKey,rreqCache[i].publicKey)==TRUE)){
				if(rreqCache[i].send==1){
					return FAIL;
				}
				else{	
					rreqCache[i].send=1; 
				}
				break;
		    }

		}
	    if(i>=AODV_RQCACHE_SIZE){
			
			return FAIL;
		}
		
		
		return SUCCESS;  // always success for now
    }


	//
    // Purpose: Start the AODV module
    // Returns: Always 1
    command result_t Control.init() {
		int i;
	
		dbg(DBG_BOOT, "AODV_Core initializing\n");

	      
		rreqMsg = &msgBuf1;
		rReplyMsg = &msgBuf2;
		rReplyMsg2 = &msgBuf7;
		rErrMsg = &msgBuf3;
		daddMsg = &msgBuf4;
		naddMsg = &msgBuf5;
		nadd_ackMsg = &msgBuf6;
		
#if RREQ_RANDOMIZE
		rreqRandomize = 0;
#endif
		rreqTaskPending = TASK_DONE;
		rreplyTaskPending = TASK_DONE;
		rreplyTaskPending2 = TASK_DONE;
		rerrTaskPending = TASK_DONE;
		daddTaskPending = TASK_DONE;
		naddTaskPending = TASK_DONE;
		nadd_ackTaskPending = TASK_DONE;
		sendPending = FALSE;

		rreqNumTries = 0;
		rreplyNumTries = 0;
		rreplyNumTries2 = 0;
		rerrNumTries = 0;
		daddNumTries = 0;
		naddNumTries = 0;
		nadd_ackNumTries = 0;
		
		DEFAULT_RREQ_EXTENSION=RREQ_SSE;
		DEFAULT_FLAGS=D_FLAG;
	
		rreqDest  =  INVALID_NODE_ID;

		rReplyDest  =  INVALID_NODE_ID;
		rReplySrc  = INVALID_NODE_ID;

		rErrDest = INVALID_NODE_ID;
		rErrSrc  = INVALID_NODE_ID;
		
		seq = 0;
		rreqID = 0;

		for(i = 0; i< AODV_RTABLE_SIZE; i++){
		    routeTable[i].dest    = INVALID_NODE_ID;
		    routeTable[i].nextHop = INVALID_NODE_ID;
		    routeTable[i].destSeq = 0;
		    routeTable[i].numHops = 0;
		}


		for(i = 0; i< AODV_RQCACHE_SIZE; i++){
		    rreqCache[i].dest    = INVALID_NODE_ID;
		    rreqCache[i].nextHop = INVALID_NODE_ID;
		    rreqCache[i].destSeq = 0;
		    rreqCache[i].numHops = 0;
			rreqCache[i].send = 0;
		}

	
		call RadioControl.init();
		call ForwardingControl.init();
		call Random.init();
		return SUCCESS;
    }
   
	
    command result_t Control.start() {
		call RadioControl.start();
		call ForwardingControl.start();
		call CRIPTO.RSAGenerateKeys(&pk,call Random.rand(), call Random.rand());
		return call Timer.start(TIMER_REPEAT, (CLOCK_SCALE/4));
	}
    
    command result_t Control.stop() {
      
		call ForwardingControl.stop();
		call RadioControl.stop();
		return call Timer.stop();
    }


    uint8_t getCacheIndex(wsnAddr src, wsnAddr dest){
		int i;
		for(i=0; i< AODV_RQCACHE_SIZE; i++){
		    if(rreqCache[i].src == src && rreqCache[i].dest == dest){
			return i;
		    }
		    return INVALID_INDEX;
		}
    }
	
	uint8_t checkRreqCache(wsnAddr src,uint32_t publicKey[2]){
		int i;
		for(i=0; i< AODV_RQCACHE_SIZE; i++){
		    if(rreqCache[i].src == src && compareSignatures(publicKey,rreqCache[i].publicKey) == FALSE){
			return i;
		    }
		   
		}
		
		return INVALID_INDEX;
    }
	
    result_t removeRTable(int indx){
      int i;

      for(i = indx; i< AODV_RTABLE_SIZE-1; i++) {
	if(routeTable[i+1].dest == INVALID_NODE_ID){
	  break;
	}
	routeTable[i] = routeTable[i+1];
      }

      routeTable[i].dest    = INVALID_NODE_ID;
      routeTable[i].nextHop = INVALID_NODE_ID;
      routeTable[i].destSeq = 0;
      routeTable[i].numHops = 0;
      return SUCCESS;
    }

    uint8_t getRTableIndex(wsnAddr dest){
	    int i;
		for(i=0; i< AODV_RTABLE_SIZE; i++){
			if(routeTable[i].dest == dest){
				return i;
			}
		}
		return INVALID_INDEX;
      
    }
    
#if AODV_CORE_DEBUG
    void printRTableEntry(int i){
       dbg(DBG_USR3, 
	   "AODV_Core: indx=%d: dest=%d nextHop=%d destSeq=%d numHops=%d \n", 
	   i, routeTable[i].dest, routeTable[i].nextHop, 
	   routeTable[i].destSeq, routeTable[i].numHops );
    }
    void printRTable(){
      int i;
      dbg(DBG_USR3, "Printing RTable entries \n");
      for(i = 0; i<AODV_RTABLE_SIZE; i++){
	if(routeTable[i].dest !=INVALID_NODE_ID){
	  printRTableEntry(i);
	}
      }
      dbg(DBG_USR3, "Printing RTable entries DONE \n");
    }

#endif
	result_t addRtableEntry(wsnAddr dest,wsnAddr src,uint32_t destSeq,uint32_t hopcount,uint32_t lifetime,uint32_t key[],uint32_t signature[],uint32_t hash[],uint32_t topHash[],uint8_t hash_function,uint32_t paddLength,uint8_t h_and_reserved,uint8_t signMethod){
	
		int i;
		if(dest == TOS_LOCAL_ADDRESS){
		    return SUCCESS; 
		}
		for(i = 0; i<AODV_RTABLE_SIZE; i++){
		    if(routeTable[i].dest == INVALID_NODE_ID){
				break;
		    }
		    if(routeTable[i].dest == dest){
				
				if(compareSignatures(routeTable[i].publicKey,key)==TRUE){
					break;
				}
				else{
										
				}
		    }
		}
		
		if(i < AODV_RTABLE_SIZE){
		    if(routeTable[i].dest == INVALID_NODE_ID  || routeTable[i].destSeq < destSeq || (routeTable[i].destSeq == destSeq  && routeTable[i].numHops < hopcount)) {
				routeTable[i].dest = dest;
				routeTable[i].RREP_originator = src;
				routeTable[i].destSeq = destSeq;
				routeTable[i].nextHop = rReplySrc;
				routeTable[i].numHops = hopcount;
				routeTable[i].lifetime=lifetime;
				arraychunkcpy(routeTable[i].signature,signature,(signature[0] & 0x0ff)+1,0);	
				arraychunkcpy(routeTable[i].publicKey,key,(key[0] & 0x0ff)+1,0);
				arraychunkcpy(routeTable[i].hash,hash,5,0);
				arraychunkcpy(routeTable[i].topHash,topHash,5,0);
				return SUCCESS;     
		    }
			
		    if(routeTable[i].destSeq == destSeq && routeTable[i].numHops == hopcount){
				// entry already exists
				return SUCCESS;
		    }
		}
		
		dbg(DBG_TEMP, ("ADDRTABLE failed\n"));	
		return FAIL;

    }

	task void sendRerr(){
		SAODV_Rerr_Signature_Extension_MsgPtr msg;
		int i;
		int aux;
		int lon;
		uint32_t message[30];
		
		dbg(DBG_ROUTE, ("SAODV_Core task sendRerr\n"));	
		call RerrPayload.linkPayload(rErrMsg, (uint8_t **) &msg);
#if AODV_CORE_DEBUG      
		printRTable();
#endif

	
		msg->type=AODV_RRER;
		msg->n_and_reserved=0;
		msg->destCount=1;
 		msg->dest      = rErrDest;

		i = getRTableIndex(msg->dest);
		
		if(i == INVALID_NODE_ID){
			rerrTaskPending = TASK_DONE;
			return;
		}
		
		msg->destSeq = routeTable[i].destSeq;  
		msg->dest2=INVALID_NODE_ID;
		msg->destSeq2=0;
		msg->type_extension=SAODV_RERR_SIGNATURE_EXTENSION;
		msg->length=56;
  		msg->reserved=0;
		msg->signMethod=DEFAULT_SIGN_METHOD;
		msg->h_and_reserved=0;
		msg->paddLength=((uint8_t) call Random.rand())%255;
		
		keycpy(msg->publicKey); 
		
		if(msg->paddLength %32 !=0){
			msg->paddLength = msg->paddLength+32;
		}
		
		msg->paddLength=msg->paddLength/32;
		
		for(aux=0;aux<msg->paddLength;aux++){
			msg->padding[aux]=0;
		}


		message[0]=msg->type;
		message[0]=message[0]<<16 | msg->n_and_reserved;
		message[0]=message[0]<<8 |msg->destCount;
		message[1]=msg->dest;
		message[2]=msg->destSeq;
		message[3]=msg->dest2;
		message[4]=msg->destSeq2;
		message[5]=msg->type_extension;
		message[5]=message[5]<<24 | 0;
		message[6]=msg->signMethod;
		message[6]=message[6]<<16 |msg->h_and_reserved;
		message[6]=message[6]<<8 |msg->paddLength;
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]&0x0ff)+1,7);
		lon=(msg->publicKey[0]&0x0ff)+1;
		arraychunkcpy(message,msg->padding,msg->paddLength,7+lon); 
		lon=lon+msg->paddLength;
		
		signMessage(7+lon,message,msg->publicKey,SHA_1,msg->signature);
		lon=lon+(msg->signature[0]& 0x0ff)+1;
		removeRTable(i); 

		if (!sendPending && call SendRerr.send(TOS_BCAST_ADDR,SAODV_RERR_HEADER_LEN, rErrMsg)) {
			sendPending = TRUE;
			rerrTaskPending = TASK_DONE;
	    }
		else{
			rerrNumTries = AODVR_NUM_TRIES;
			rerrTaskPending = TASK_REPOSTREQ;
			return;
		}
#if AODV_CORE_DEBUG      
      dbg(DBG_ROUTE, "SAODV_Core task sendRerr taskPending = %d \n",rerrTaskPending);	
      printRTable();
#endif
      
    }

	int signNadd_ack(SAKM_Nadd_ack_MsgPtr msg){
		int lon;
		uint32_t message[20];
		lon=0;
		message[0]=msg->type;
		message[0]=message[0]<<24 | msg->reserved;
		message[1]=msg->old_ip;
		message[2]=msg->new_ip;
		message[3]=msg->signMethod;
		message[3]=message[3]<<16 |msg->h_and_reserved;
		message[3]=message[3]<<8 |msg->paddLength;
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]&0x0ff)+1,4);
		lon=lon+(msg->publicKey[0]&0x0ff)+1;
		arraychunkcpy(message,msg->padding,msg->paddLength,4+lon); 
		lon=lon+msg->paddLength;		
		signMessage(4+lon,message,msg->publicKey,SHA_1,msg->signature);
		return 44;
	}
	
	task void sendNadd_ack(){
		SAKM_Nadd_ack_MsgPtr msg;
		int aux;
		dbg(DBG_ROUTE, ("SAODV_Core task sendNadd_ack\n"));	
		call Nadd_ackPayload.linkPayload(nadd_ackMsg, (uint8_t **) &msg);
#if AODV_CORE_DEBUG      
		printRTable();
#endif

		msg->type=SAKM_NADD_ACK;
		msg->length=0;
	  	msg->reserved=0;
		msg->old_ip=nadd_old_ip;
		msg->new_ip=nadd_new_ip;
					
		msg->signMethod=DEFAULT_SIGN_METHOD;
		msg->h_and_reserved=0;
		msg->paddLength=1;//((uint8_t) call Random.rand())%255;

		if(msg->paddLength %32 !=0){
				msg->paddLength = msg->paddLength+32;
			}
			
		msg->paddLength=msg->paddLength/32;
			
		
		for(aux=0;aux<msg->paddLength;aux++){
				msg->padding[aux]=0;
			}
		keycpy(msg->publicKey);
						
		msg->length=signNadd_ack(msg);
						
		if (!sendPending && call SendNadd_ack.send(nadd_ackDest,SAKM_NADD_ACK_HEADER_LEN, nadd_ackMsg)) {
				sendPending = TRUE;
				nadd_ackTaskPending = TASK_DONE;
		    }
		else{
				nadd_ackNumTries = AODVR_NUM_TRIES;
				nadd_ackTaskPending = TASK_REPOSTREQ;
				return;
			}
			
#if AODV_CORE_DEBUG      
      dbg(DBG_ROUTE, "SAKM task sendNadd_ack taskPending = %d \n",nadd_ackTaskPending);	
      printRTable();
#endif
      
    }
	
	int signNadd(SAKM_Nadd_MsgPtr msg){
		int lon;
		uint32_t message[20];
		lon=0;
		
		message[0]=msg->type;
		message[0]=message[0]<<24 | msg->reserved;
		message[1]=msg->signMethod;
		message[1]=message[1]<<16 | msg->h_and_reserved;
		message[1]=message[1]<<8 | msg->paddLength;
		message[2]=msg->old_ip;
		message[3]=msg->new_ip;
		arraychunkcpy(message,msg->oldpublicKey,(msg->oldpublicKey[0]&0x0ff)+1,4);
		lon=lon+(msg->oldpublicKey[0]&0x0ff)+1;
		arraychunkcpy(message,msg->padding,msg->paddLength,4+lon); 
		lon=lon+msg->paddLength;
		signMessage(4+lon,message,msg->oldpublicKey,SHA_1,msg->signature_old_key);
	
		lon=0;
		message[1]=msg->signMethod2;
		message[1]=message[1]<<16 |msg->h_and_reserved2;
		message[1]=message[1]<<8 |msg->paddLength2;
		message[2]=msg->old_ip;
		message[3]=msg->new_ip;
		arraychunkcpy(message,msg->publicKey,(msg->publicKey[0]&0x0ff)+1,4);
		lon=lon+(msg->publicKey[0]&0x0ff)+1;
		arraychunkcpy(message,msg->padding2,msg->paddLength2,4+lon); 
		lon=lon+msg->paddLength2;
		signMessage(4+lon,message,msg->publicKey,SHA_1,msg->signature_new_key);
		lon=lon+(msg->signature_new_key[0]& 0x0ff)+1+(msg->signature_old_key[0]& 0x0ff)+1;
		
		return 79;
	}
	
	task void sendNadd(){
		SAKM_Nadd_MsgPtr msg;
		int i;
		int aux;
		uint8_t k[4];
		uint8_t out[20];
		call NaddPayload.linkPayload(naddMsg, (uint8_t **) &msg);
#if AODV_CORE_DEBUG      
		dbg(DBG_ROUTE, ("SAKM task sendNadd\n"));	
		printRTable();
#endif
		naddTaskPending = TASK_DONE;
		msg->type=SAKM_NADD;
		msg->length=0;
  		msg->reserved=0;
		msg->signMethod=DEFAULT_SIGN_METHOD;
		msg->h_and_reserved=0;
		msg->paddLength=((uint8_t) call Random.rand())%255;

		if(msg->paddLength %32 !=0){
			msg->paddLength = msg->paddLength+32;
		}
		
		msg->paddLength=msg->paddLength/32;
	
		for(aux=0;aux<msg->paddLength;aux++){
			msg->padding[aux]=0;
		}

		
		keycpy(msg->oldpublicKey);
		msg->signMethod2=DEFAULT_SIGN_METHOD;
		msg->h_and_reserved2=0;
		msg->paddLength2=1;//((uint8_t) call Random.rand())%255;
		
		call CRIPTO.RSAGenerateKeys(&pk,call Random.rand(), call Random.rand());
		keycpy(msg->publicKey);
		
		if(msg->paddLength2 %32 !=0){
			msg->paddLength2 = msg->paddLength2+32;
		}
		
		msg->paddLength2=msg->paddLength2/32;
		
		
		for(aux=0;aux<msg->paddLength2;aux++){
			msg->padding2[aux]=0;
		}
		msg->old_ip=TOS_LOCAL_ADDRESS;
		
		for(i=1;i<((msg->publicKey[0]&0x0ff)+1);i++){
		
			k[(i*4)]=msg->publicKey[i]>>24;
			k[(i*4)+1]=msg->publicKey[i]>>16 & 0x0FF;
			k[(i*4)+2]=msg->publicKey[i]>>8 & 0x0ff;
			k[(i*4)+3]=msg->publicKey[i] & 0x0FF;
	
		}
		call CRIPTO.hmac_sha( k,4, k, 4,out);
		atomic{TOS_LOCAL_ADDRESS=out[0];}
		msg->new_ip=TOS_LOCAL_ADDRESS;
		msg->length=signNadd(msg);
	
		if (!sendPending && call SendNadd.send(TOS_BCAST_ADDR,SAKM_NADD_HEADER_LEN, naddMsg)) {
			sendPending = TRUE;
			naddTaskPending = TASK_DONE;
	    }
		else{
			naddNumTries = AODVR_NUM_TRIES;
			naddTaskPending = TASK_REPOSTREQ;
			return;
		}
		//test sakm rErrDest=msg->old_ip;
		//test sakm post sendRerr();
		
#if AODV_CORE_DEBUG      
      dbg(DBG_ROUTE, "SAKM task sendNadd taskPending = %d \n",naddTaskPending);	
      printRTable();
#endif
      
    }
	
	task void sendDadd(){
		SAKM_Dadd_MsgPtr msg;
#if AODV_CORE_DEBUG		
		dbg(DBG_ROUTE, ("SAKM task sendDadd\n"));
#endif		
		call DaddPayload.linkPayload(daddMsg, (uint8_t **) &msg);

		if(daddSrc == INVALID_NODE_ID){
			daddTaskPending = TASK_DONE;
			return;
		}

		msg->type=SAKM_DADD;
		msg->length=0;
  		msg->h_and_reserved=0;
		msg->duplicated_node_ip_address[0]=daddSrc;
		arraychunkcpy(msg->duplicated_node_public_key,rreqCache[position].publicKey,(rreqCache[position].publicKey[0]&0x0ff)+ 1,0);
				
		msg->length= 16; 
		

		if (!sendPending && call SendDadd.send(rreqCache[position].nextHop,SAKM_DADD_HEADER_LEN, daddMsg)) {
			
			sendPending = TRUE;
			daddTaskPending = TASK_DONE;
	    }
		else{
			daddNumTries = AODVR_NUM_TRIES;
			daddTaskPending = TASK_REPOSTREQ;
			return;
		}
		
		
#if AODV_CORE_DEBUG      
	   dbg(DBG_ROUTE, "SAKM task sendDadd taskPending = %d \n",daddTaskPending);	
#endif
      
    }
	
	task void fwdDadd(){
	    SAKM_Dadd_MsgPtr msg;
		int i;
		dbg(DBG_ROUTE, ("SAKM task fwdDadd\n"));	
     
/*#if AODV_CORE_DEBUG      
		printRTable();
#endif*/
		call DaddPayload.linkPayload(daddMsg, (uint8_t **) &msg);
		i = checkRreqCache(msg->duplicated_node_ip_address[0],msg->duplicated_node_public_key);
		
		if(i != INVALID_INDEX  /*&& routeTable[i].nextHop == msg->duplicated_node_ip_address[0]*/){

			if (!sendPending && call SendRerr.send(rreqCache[i].nextHop, SAKM_DADD_HEADER_LEN, daddMsg)) {
				sendPending = TRUE;
				daddTaskPending = TASK_DONE;
			}
			else{
				daddNumTries = AODVR_NUM_TRIES;
				daddTaskPending = TASK_REPOSTREQ;
				return;
			}
		}
		else{
			daddTaskPending = TASK_DONE;
		}
		
		dbg(DBG_ROUTE, "SAKM task fwddadd taskPending = %d \n",daddTaskPending);	
/*#if AODV_CORE_DEBUG      
		
		printRTable();
#endif*/
 	}
		
	task void fwdRerr() {
	    SAODV_Rerr_Signature_Extension_MsgPtr msg;
		int i;
		dbg(DBG_ROUTE, ("AODV_Core task fwdRerr\n"));	
     
#if AODV_CORE_DEBUG      
		printRTable();
#endif
		call RerrPayload.linkPayload(rErrMsg, (uint8_t **) &msg);
		i = getRTableIndex(rErrDest);

		if(i != INVALID_INDEX && routeTable[i].nextHop == rErrSrc){
			msg->dest = rErrDest;
			msg->destSeq = routeTable[i].destSeq;
			removeRTable(i);

			if (!sendPending && call SendRerr.send(TOS_BCAST_ADDR, SAODV_RERR_HEADER_LEN, rErrMsg)) {
				sendPending = TRUE;
				rerrTaskPending = TASK_DONE;
			}
			else{
				rerrNumTries = AODVR_NUM_TRIES;
				rerrTaskPending = TASK_REPOSTREQ;
				return;
			}
		}
		else{
			rerrTaskPending = TASK_DONE;
		}
#if AODV_CORE_DEBUG      
		dbg(DBG_ROUTE, "AODV_Core task fwdRerr taskPending = %d \n",rerrTaskPending);	
		printRTable();
#endif
    }
	
    task void resendRerr(){
		if(rerrNumTries <= 0){
			rerrTaskPending = TASK_DONE;
			return;
		}
		if (!sendPending && call SendRerr.send(TOS_BCAST_ADDR,SAODV_RERR_HEADER_LEN, rErrMsg)) {
			sendPending = TRUE;
			rerrTaskPending = TASK_DONE;
		}
		else{
			rerrNumTries--;
			rerrTaskPending = TASK_REPOSTREQ;
		}
    }
	
	void generateRreqSingle(){
		int lon;
		uint32_t message[30];

			message[0]=rreqSingleMsg->type;
			message[0]=message[0]<<16 | rreqSingleMsg->jrgdu_and_reserved;
			message[0]=message[0]<<8 | 0;
			
			
			message[1]=rreqSingleMsg->rreqID;
			message[2]=rreqSingleMsg->dest;
			message[3]=rreqSingleMsg->destSeq;
			message[4]=rreqSingleMsg->src;
			message[5]=rreqSingleMsg->srcSeq;
			message[6]=rreqSingleMsg->type_extension;
			message[6]=message[6]<<16| rreqSingleMsg->hash_function;
			message[6]=message[6]<<8|rreqSingleMsg->maxHopCount;
			
			lon=SHA1HashSize/4;
			arraychunkcpy(message,rreqSingleMsg->topHash,lon,7);
			
			message[7+lon]=rreqSingleMsg->signMethod;
			message[7+lon]=message[7+lon]<<16 |rreqSingleMsg->h_and_reserved;
			message[7+lon]=message[7+lon]<<8|rreqSingleMsg->paddLength;
			
			arraychunkcpy(message,rreqSingleMsg->publicKey,(rreqSingleMsg->publicKey[0]& 0x0ff) + 1,8+lon);
			lon=lon+(rreqSingleMsg->publicKey[0] & 0x0ff)+1;
			
			arraychunkcpy(message,rreqSingleMsg->padding,(rreqSingleMsg->paddLength/32),8+lon);
			lon=lon+(rreqSingleMsg->paddLength/32);	
		
		signMessage(8+ lon,message,rreqSingleMsg->publicKey,rreqSingleMsg->hash_function,rreqSingleMsg->signature);
	
		
			if (!sendPending && call SendRreq.send(TOS_BCAST_ADDR, SAODV_RREQ_SINGLE_HEADER_LEN, rreqMsg)) {
			    sendPending = TRUE;
			    rreqTaskPending = TASK_DONE;
			}
			else{
				rreqNumTries = AODVR_NUM_TRIES;
				rreqTaskPending = TASK_REPOSTREQ;
			}
	
	}
	
	void generateRreqDouble(){
		int lon;
		uint32_t message[50];
	
		message[0]=rreqDoubleMsg->type;
		message[0]=message[0]<<16 |rreqDoubleMsg->jrgdu_and_reserved;
		message[0]=message[0]<<8;
		message[1]=rreqDoubleMsg->rreqID;
		message[2]=rreqDoubleMsg->dest;
		message[3]=0;
		message[4]=rreqDoubleMsg->src;
		message[5]=rreqDoubleMsg->srcSeq;
		message[6]=rreqDoubleMsg->type_extension;
		message[6]=message[6]<<16|rreqDoubleMsg->hash_function;
		message[6]=message[6]<<8|rreqDoubleMsg->maxHopCount;
		message[7]=0;
		message[8]=rreqDoubleMsg->signMethod;
		message[8]=message[8]<<16 |0;
		message[8]=message[8]<<8|rreqDoubleMsg->paddLength;
		lon=SHA1HashSize/4;
		arraychunkcpy(message,rreqDoubleMsg->topHash,lon,9);
		arraychunkcpy(message,rreqDoubleMsg->publicKey,(rreqDoubleMsg->publicKey[0]& 0x0ff )+1 ,9+lon);
		lon=lon+((rreqDoubleMsg->publicKey[0] & 0x0ff)+1);

		arraychunkcpy(message,rreqDoubleMsg->signatureRREP,(rreqDoubleMsg->signatureRREP[0]&0x0ff)+1,9+lon);
		lon=lon+(rreqDoubleMsg->signatureRREP[0]&0x0ff)+1;
		signMessage(9+lon,message,rreqDoubleMsg->publicKey,rreqDoubleMsg->hash_function,rreqDoubleMsg->signature);

		if (!sendPending && call SendRreq.send(TOS_BCAST_ADDR, SAODV_RREQ_DOUBLE_HEADER_LEN, rreqMsg)) {
		    sendPending = TRUE;
		    rreqTaskPending = TASK_DONE;
		}
		else{
		    rreqNumTries = AODVR_NUM_TRIES;
		    rreqTaskPending = TASK_REPOSTREQ;
		}
	}
	
	task void sendRreq() {
		
	    int aux;
		
		int i;
		int lon;

		uint32_t hash_aux[5];
		dbg(DBG_USR1,"SAODV: sendRREQ\n");
		
		seq++;
		rreqID++;
	      
		if(DEFAULT_RREQ_EXTENSION==RREQ_SSE){
			call RreqPayload.linkPayload(rreqMsg, (uint8_t **) &rreqSingleMsg);
			rreqSingleMsg->type=AODV_RREQ;
			rreqSingleMsg->jrgdu_and_reserved=DEFAULT_FLAGS;
			rreqSingleMsg->metric[0]=0;
			
			rreqSingleMsg->rreqID=rreqID;
			rreqSingleMsg->dest      = 0;
			rreqSingleMsg->destSeq   = 0 ;   // this function only sends rreq's if no route exists
			rreqSingleMsg->src       = TOS_LOCAL_ADDRESS; 
			rreqSingleMsg->srcSeq    = seq;
			
			rreqSingleMsg->type_extension=SAODV_RREQ_SINGLE_SIGNATURE_EXTENSION;  
			rreqSingleMsg->length=97;
			rreqSingleMsg->hash_function=DEFAULT_HASH_FUNCTION;
			rreqSingleMsg->maxHopCount=DEFAULT_MAX_HOP_COUNT;//TTL FROM IP HEADER
			rreqSingleMsg->signMethod=DEFAULT_SIGN_METHOD;
			rreqSingleMsg->h_and_reserved=0;
			rreqSingleMsg->paddLength=(uint8_t) (call Random.rand()%255);

			lon=SHA1HashSize/4;
			
			for(i=0;i<lon;i++){
				rreqSingleMsg->hash[i]=0;hash_aux[i]=0;
			}
			
			rreqSingleMsg->hash[lon-1]=call Random.rand();
			hash_aux[lon-1]=rreqSingleMsg->hash[lon-1];
						
			calculateHash2(rreqSingleMsg->maxHopCount,rreqSingleMsg->hash,5,rreqSingleMsg->hash_function);
			arraychunkcpy(rreqSingleMsg->topHash,res,SHA1HashSize/4,0);			
			calculateHash2(0,rreqSingleMsg->hash,5,rreqSingleMsg->hash_function);
			arraychunkcpy(rreqSingleMsg->hash,res,SHA1HashSize/4,0);

			if(rreqSingleMsg->paddLength %32 !=0){
				rreqSingleMsg->paddLength=rreqSingleMsg->paddLength+32;
			}
			
			rreqSingleMsg->paddLength=rreqSingleMsg->paddLength/32;
			for(aux=0;aux<rreqSingleMsg->paddLength;aux++){
				rreqSingleMsg->padding[aux]=0;
			}
		
			keycpy(rreqSingleMsg->publicKey);

			generateRreqSingle();
		}
		else{
			
			call RreqPayload.linkPayload(rreqMsg, (uint8_t **) &rreqDoubleMsg);
			rreqDoubleMsg->type=AODV_RREQ;
			rreqDoubleMsg->jrgdu_and_reserved=DEFAULT_FLAGS;
			rreqDoubleMsg->metric[0]=0;
			
			rreqDoubleMsg->rreqID=rreqID;
			rreqDoubleMsg->dest      = 0;
			rreqDoubleMsg->destSeq   = 0 ;   // this function only sends rreq's if no route exists
			rreqDoubleMsg->src       = TOS_LOCAL_ADDRESS; 
			rreqDoubleMsg->srcSeq    = seq;
			
			rreqDoubleMsg->type_extension=SAODV_RREQ_DOUBLE_SIGNATURE_EXTENSION;  
			rreqDoubleMsg->length=123;
			rreqDoubleMsg->hash_function=DEFAULT_HASH_FUNCTION;
			rreqDoubleMsg->maxHopCount=DEFAULT_MAX_HOP_COUNT;//TTL FROM IP HEADER
			lon=SHA1HashSize/4;
			
			for(i=0;i<lon;i++){
				rreqDoubleMsg->hash[i]=0;
			}
			
			rreqDoubleMsg->hash[lon-1]=call Random.rand();
			calculateHash2(rreqDoubleMsg->maxHopCount,rreqDoubleMsg->hash,lon,rreqDoubleMsg->hash_function);
			arraychunkcpy(rreqDoubleMsg->topHash,res,lon,0);
			
			rreqDoubleMsg->signMethod=DEFAULT_SIGN_METHOD;
			rreqDoubleMsg->h_and_reserved=0;
			rreqDoubleMsg->paddLength=1;//(uint8_t) (call Random.rand()%255);
			if(rreqDoubleMsg->paddLength %32 !=0){
				rreqDoubleMsg->paddLength=rreqDoubleMsg->paddLength+32;
			}
			
			rreqDoubleMsg->paddLength=rreqDoubleMsg->paddLength/32;
			/*for(aux=0;aux<rreqDoubleMsg->paddLength;aux++){
				rreqDoubleMsg->padding[aux]=0;
			}
			*/
			keycpy(rreqDoubleMsg->publicKey);		
			generateRREP(rreqDoubleMsg->dest,rreqDoubleMsg->destSeq,rreqDoubleMsg->signatureRREP,rreqDoubleMsg->hash,rreqDoubleMsg->topHash,rreqDoubleMsg->paddLength);
			calculateHash2(0,rreqDoubleMsg->hash,5,rreqDoubleMsg->hash_function);
	
			generateRreqDouble();
		}
		
	      
    }
	
	task void forwardRreqSingle() {
		SAODV_Rreq_Single_Signature_MsgPtr msg;
	#if AODV_CORE_DEBUG
		dbg(DBG_USR1,"SAODV forward RREQ-SSE\n");
#endif       
		call RreqPayload.linkPayload(rreqMsg, (uint8_t **) &msg);
	      
		*msg = fwdRreq;
		msg->metric[0]++;  // simple hop metric for now
	
		calculateHash2(1,msg->hash,5,msg->hash_function);
		arraychunkcpy(msg->hash,res,SHA1HashSize/4,0);
	
#if RREQ_RANDOMIZE      
		if(rreqRandomize = ((call Random.rand() & 0xff) % AODV_MAX_RAND)){
		    rreqNumTries = AODVR_NUM_TRIES;
		    rreqTaskPending = TASK_REPOSTREQ;
		    return;
		}
#endif
		if (!sendPending && call SendRreq.send(TOS_BCAST_ADDR,SAODV_RREQ_SINGLE_HEADER_LEN, rreqMsg)) {
		    sendPending = TRUE;
		    rreqTaskPending = TASK_DONE;
		}  
		else{
			rreqNumTries = AODVR_NUM_TRIES;
			rreqTaskPending = TASK_REPOSTREQ;
		}
    }

	task void forwardRreqDouble() {
		SAODV_Rreq_Double_Signature_MsgPtr msg;
	      
		call RreqPayload.linkPayload(rreqMsg, (uint8_t **) &msg);
#if AODV_CORE_DEBUG
		dbg(DBG_USR1,"SAODV forward RREQ-DSE\n");
#endif   
		
		
		*msg = fwdRreqDouble;
		msg->metric[0]++;  // simple hop metric for now
		calculateHash2(1,msg->hash,5,msg->hash_function);
		arraychunkcpy(msg->hash,res,SHA1HashSize/4,0);

#if RREQ_RANDOMIZE      
		if(rreqRandomize = ((call Random.rand() & 0xff) % AODV_MAX_RAND)){
		    rreqNumTries = AODVR_NUM_TRIES;
		    rreqTaskPending = TASK_REPOSTREQ;
		    return;
		}
#endif
		if (!sendPending && call SendRreq.send(TOS_BCAST_ADDR,SAODV_RREQ_DOUBLE_HEADER_LEN, rreqMsg)) {
		    sendPending = TRUE;
		    rreqTaskPending = TASK_DONE;
		}  
		else{
		  rreqNumTries = AODVR_NUM_TRIES;
		  rreqTaskPending = TASK_REPOSTREQ;
		}
    }
	
    task void resendRreq(){
		
		SAODV_Rreq_MsgPtr msg;
		uint32_t lon;
		if(rreqNumTries <= 0){
			rreqTaskPending = TASK_DONE;
			return;
		}
#if RREQ_RANDOMIZE
		if(rreqRandomize >0){
			dbg(DBG_USR3, "Calling resendRreq Randomizetries = %d\n", rreqRandomize); 
			rreqTaskPending = TASK_REPOSTREQ;
			rreqRandomize--;
			return;
		}
#endif
		dbg(DBG_ROUTE, "Calling resendRreq tries = %d\n", rreqNumTries);	      
		call RreqPayload.linkPayload(rreqMsg, (uint8_t **) &msg);
		
		if(msg->type_extension==SAODV_RREQ_SINGLE_SIGNATURE_EXTENSION){
			lon=SAODV_RREQ_SINGLE_HEADER_LEN;
		}
		else{
			lon=SAODV_RREQ_DOUBLE_HEADER_LEN;
		}
		if (!sendPending && call SendRreq.send(TOS_BCAST_ADDR, lon, rreqMsg)) {
			sendPending = TRUE;
			rreqTaskPending = TASK_DONE;
		}
		else{
			rreqNumTries--;
			rreqTaskPending = TASK_REPOSTREQ;
		}
    }

	task void forwardRreplySingle(){
		SAODV_Rreply_Single_Signature_MsgPtr msg;
	      
		call RreplyPayload.linkPayload(rReplyMsg, (uint8_t **) &msg);
		dbg(DBG_ROUTE, ("Calling fwdRreplySingle \n"));	
		*msg = fwdRreply;
		
		msg->metric[0]++;  // simple hop metric for now
		calculateHash2(1,msg->hash,5,msg->hash_function);
		arraychunkcpy(msg->hash,res,SHA1HashSize/4,0);
		
		
		
		
#if AODV_CORE_DEBUG
		printRTable();
#endif     
	 	
		if(!addRtableEntry(msg->dest,msg->src,msg->destSeq,msg->metric[0],msg->lifetime,msg->publicKey,msg->signature,msg->hash,msg->topHash,msg->hash_function,msg->paddLength,msg->H_and_Reserved,msg->signMethod)){
		  rreplyTaskPending = TASK_DONE;
		  return;
		}
		if (!sendPending && call SendRreply.send(rReplyDest, SAODV_RREPLY_SINGLE_HEADER_LEN,  rReplyMsg)) {
		    dbg(DBG_ROUTE, ("Calling SendRreply successful\n"));	
		    sendPending = TRUE;
		    rreplyTaskPending = TASK_DONE;
			    
		}
		else{
		  rreplyTaskPending = TASK_REPOSTREQ;
		  rreplyNumTries = AODVR_NUM_TRIES;
		}
#if AODV_CORE_DEBUG
		printRTable();
#endif      
	      
    }
	
	
	task void forwardRreplyDouble(){
		SAODV_Rreply_Double_Signature_MsgPtr msg;
				
		call RreplyPayload.linkPayload(rReplyMsg, (uint8_t **) &msg);
#if AODV_CORE_DEBUG
		dbg(DBG_ROUTE, ("Calling fwdRreplyDouble \n"));	
#endif	
			*msg = fwdRreplyDouble;
	
		
		msg->metric[0]++;  
		calculateHash2(1,msg->hash,5,msg->hash_function);
		arraychunkcpy(msg->hash,res,SHA1HashSize/4,0);

		
#if AODV_CORE_DEBUG
		printRTable();
#endif      	
		if(!addRtableEntry(msg->dest,msg->src,msg->destSeq,msg->metric[0],msg->lifetime,msg->publicKey,msg->signature,msg->hash,msg->topHash,msg->hash_function,msg->paddLength,msg->H_and_Reserved,msg->signMethod)){ 
		  rreplyTaskPending = TASK_DONE;
		  return;
		}
		if (!sendPending && 
		    call SendRreply.send(rReplyDest, SAODV_RREPLY_DOUBLE_HEADER_LEN,  rReplyMsg)) {
		    dbg(DBG_ROUTE, ("Calling SendRreplyDSE successful\n"));	
		    if(num_rrep==2){num_rrep=1;
				rreplyTaskPending = TASK_PENDING;
			}
			else{
				sendPending = TRUE;
				rreplyTaskPending = TASK_DONE;
			}
			
			
			    
		}
		else{
		  rreplyTaskPending = TASK_REPOSTREQ;
		  rreplyNumTries = AODVR_NUM_TRIES;
		}
#if AODV_CORE_DEBUG
		printRTable();
#endif      
	      
    }
    
	task void forwardRreplyDouble2(){
		SAODV_Rreply_Double_Signature_MsgPtr msg;

		call RreplyPayload.linkPayload(rReplyMsg2, (uint8_t **) &msg);
#if AODV_CORE_DEBUG
		dbg(DBG_ROUTE, ("Calling fwdRreplyDouble2 \n"));	
#endif	
		*msg = fwdRreplyDouble2;
		msg->metric[0]++;  
		calculateHash2(1,msg->hash,5,msg->hash_function);
		arraychunkcpy(msg->hash,res,SHA1HashSize/4,0);

		
#if AODV_CORE_DEBUG
		printRTable();
#endif      	

		if (!sendPending && 
		    call SendRreply.send(rReplyDest, SAODV_RREPLY_DOUBLE_HEADER_LEN,  rReplyMsg2)) {
		    dbg(DBG_ROUTE, ("Calling SendRreplyDSE successful\n"));	
		    if(num_rrep==2){num_rrep=1;
				rreplyTaskPending = TASK_PENDING;
			}
			else{
				sendPending = TRUE;
				rreplyTaskPending = TASK_DONE;
			}
			
			
			    
		}
		else{
		  rreplyTaskPending = TASK_REPOSTREQ;
		  rreplyNumTries = AODVR_NUM_TRIES;
		}    
	      
    }
    
	
	
	task void resendRreply(){
		SAODV_Rreply_MsgPtr msg;
		uint32_t lon;
				call RreplyPayload.linkPayload(rReplyMsg, (uint8_t **) &msg);
		if(msg->type_extension==SAODV_RREP_SINGLE_SIGNATURE_EXTENSION){
			lon=SAODV_RREPLY_SINGLE_HEADER_LEN;
		}
		else{
			lon=SAODV_RREPLY_DOUBLE_HEADER_LEN;
		}
		
		if(rreplyNumTries <= 0){
			rreplyTaskPending = TASK_DONE;
			return;
		}
		if (!sendPending && call SendRreply.send(TOS_BCAST_ADDR, lon, rReplyMsg)) {
			sendPending = TRUE;
			rreplyTaskPending = TASK_DONE;
		}
		else{
			rreplyNumTries--;
			rreplyTaskPending = TASK_REPOSTREQ;
		}

    }
	task void resendRreply2(){
		SAODV_Rreply_MsgPtr msg;
		uint32_t lon;
				call RreplyPayload.linkPayload(rReplyMsg2, (uint8_t **) &msg);
		if(msg->type_extension==SAODV_RREP_SINGLE_SIGNATURE_EXTENSION){
			lon=SAODV_RREPLY_SINGLE_HEADER_LEN;
		}
		else{
			lon=SAODV_RREPLY_DOUBLE_HEADER_LEN;
		}
		
		if(rreplyNumTries2 <= 0){
			rreplyTaskPending2 = TASK_DONE;
			return;
		}
		if (!sendPending && call SendRreply.send(TOS_BCAST_ADDR, lon, rReplyMsg2)) {
			sendPending = TRUE;
			rreplyTaskPending2 = TASK_DONE;
		}
		else{
			rreplyNumTries2--;
			rreplyTaskPending2 = TASK_REPOSTREQ;
		}

    }

	task void resendDadd(){
		if(daddNumTries<=0){
			daddTaskPending=TASK_DONE;
			return;
		}
		
		if(!sendPending && call SendDadd.send(rreqCache[position].nextHop/*TOS_BCAST_ADDR*/,SAKM_DADD_HEADER_LEN,daddMsg)){
			sendPending=TRUE;
			daddTaskPending=TASK_DONE;
		}
		else{
			daddNumTries--;
			daddTaskPending=TASK_REPOSTREQ;
		}
	}

	task void fwdNadd() {
	    SAKM_Nadd_MsgPtr msg;
			
     
#if AODV_CORE_DEBUG
		dbg(DBG_ROUTE, ("SAKM task fwdNadd\n"));
		printRTable();
#endif
		call NaddPayload.linkPayload(naddMsg, (uint8_t **) &msg);
		*msg=forwardNadd;
		
			
			if (!sendPending && call SendNadd.send(TOS_BCAST_ADDR, SAKM_NADD_HEADER_LEN,naddMsg)) {
				sendPending = TRUE;
				naddTaskPending = TASK_DONE;
			}
			else{
				naddNumTries = AODVR_NUM_TRIES;
				naddTaskPending = TASK_REPOSTREQ;
				return;
			}
		
#if AODV_CORE_DEBUG      
		dbg(DBG_ROUTE, "SAKM task fwdNadd taskPending = %d \n",naddTaskPending);	
		//printRTable();
#endif
    }
		
	task void resendNadd(){
		if(naddNumTries<=0){
			naddTaskPending=TASK_DONE;
			return;
		}
		
		if(!sendPending && call SendNadd.send(TOS_BCAST_ADDR,SAKM_NADD_HEADER_LEN,naddMsg)){
			sendPending=TRUE;
			naddTaskPending=TASK_DONE;
		}
		else{
			naddNumTries--;
			naddTaskPending=TASK_REPOSTREQ;
		}
	}
	
	task void resendNadd_ack(){
		if(naddNumTries<=0){
			nadd_ackTaskPending=TASK_DONE;
			return;
		}
		
		if(!sendPending && call SendNadd_ack.send(nadd_ackDest,SAKM_NADD_ACK_HEADER_LEN,nadd_ackMsg)){
			sendPending=TRUE;
			nadd_ackTaskPending=TASK_DONE;
		}
		else{
			nadd_ackNumTries--;
			nadd_ackTaskPending=TASK_REPOSTREQ;
		}
	}
	
    // success means that the possible new entry is usable
   result_t checkCache(wsnAddr src, uint32_t IDrreq, uint32_t metric,uint32_t publicKey[]){
  
		int i;
#if  AODV_CORE_DEBUG  
		dbg(DBG_ROUTE, ("------------------------------------------AODV_Core: Printing RREQ Cache------------------------------------------\n"));
		for(i=0; i< AODV_RQCACHE_SIZE; i++){
		  if(rreqCache[i].dest == INVALID_NODE_ID){
		    break;
		  }
		  dbg(DBG_ROUTE, "AODV_Core:RREQ Cache i = %d, src = %d, rreqID = %d, numhops = %d \n", i, rreqCache[i].src, rreqCache[i].rreqID, rreqCache[i].numHops);
		}
		dbg(DBG_ROUTE, ("AODV_Core: Printing RREQ Cache DONE\n"));
#endif

		for(i=0; i< AODV_RQCACHE_SIZE; i++){
		    if(rreqCache[i].dest == INVALID_NODE_ID){
				return SUCCESS;
		    }

		    if(rreqCache[i].src == src){ 
				if(compareSignatures(rreqCache[i].publicKey,publicKey)==FALSE){
					dbg(DBG_USR1,"SAKM: ERROR duplicated address detected\n");
					daddDest=src;
					position=i;
					//test  solo sakm 
					post sendDadd();
					//test solo sakm 
					return FAIL;
				}
			
				if(rreqCache[i].rreqID < IDrreq ||  (rreqCache[i].rreqID == IDrreq &&   rreqCache[i].numHops > metric)){
						// this is a newer rreq
						return SUCCESS;
				}
				else{
					return FAIL;
				}
		    }
		}

		return SUCCESS;
    }
    
	
    result_t updateCache(wsnAddr dest,wsnAddr src, uint32_t IDrreq,uint8_t metric,uint32_t destSeq,uint32_t publicKey[], wsnAddr nextHop){
		int i;
		int endIndex = -1;
		int replaceIndex=-1;
		for(i=0; i< AODV_RQCACHE_SIZE; i++){
		    if(rreqCache[i].dest == INVALID_NODE_ID){
				endIndex = i;
			break;
		    }

		    if(rreqCache[i].src == src){
				replaceIndex = i;
				break;
		    }
		}
	       
		if(replaceIndex != -1){
		  for(i=replaceIndex; i< AODV_RQCACHE_SIZE-1; i++){
			if(rreqCache[i+1].dest != INVALID_NODE_ID){
				  rreqCache[i] = rreqCache[i+1];
			}
			else{
				  break;
			}
		  }
		}
		else{
		  if(endIndex == -1){
		    // no empty entries make room
		    for(i=0; i< AODV_RQCACHE_SIZE-1; i++){
				rreqCache[i] = rreqCache[i+1];
		  }
		    
		  }
		}
		  
		// i should be at the right place now
		rreqCache[i].dest = dest;
		rreqCache[i].src = src;
		rreqCache[i].nextHop = nextHop;
		rreqCache[i].rreqID = IDrreq;
		rreqCache[i].destSeq = destSeq;
		rreqCache[i].numHops = metric; // +1 ??
		arraychunkcpy(rreqCache[i].publicKey,publicKey,(publicKey[0] & 0x0ff)+1,0); 
#if  AODV_CORE_DEBUG  
		dbg(DBG_ROUTE, ("AODV_Core: Update Printing RREQ Cache\n"));
		for(i=0; i< AODV_RQCACHE_SIZE; i++){
		  if(rreqCache[i].dest == INVALID_NODE_ID){
		    break;
		  }
		  dbg(DBG_ROUTE, "AODV_Core:RREQ Cache i = %d, src = %d, rreqID = %d, numHops = %d \n", i, rreqCache[i].src, rreqCache[i].rreqID, rreqCache[i].numHops);
		}
		dbg(DBG_ROUTE, ("AODV_Core: Update Printing RREQ Cache DONE\n"));
#endif
		
		return SUCCESS;  // always success for now
    }

    wsnAddr getReverseRoute(SAODV_Rreply_MsgPtr msg){
	int i;
	for(i=0; i< AODV_RQCACHE_SIZE; i++){
	    if(rreqCache[i].dest == INVALID_NODE_ID){
		return INVALID_NODE_ID;
	    }
	    if(rreqCache[i].src == msg->src){
		return rreqCache[i].nextHop;
	    }
	}
	return INVALID_NODE_ID;
    }


    event result_t SendRreq.sendDone(TOS_MsgPtr sentBuffer, bool success) {

		if ((sendPending == TRUE) && (sentBuffer == rreqMsg)) {
			sendPending = FALSE;
		    return SUCCESS;
		}
		else{
			return FAIL;
		}
    }
    
    event result_t SendRreply.sendDone(TOS_MsgPtr sentBuffer,bool success) {
	    dbg(DBG_USR3, "Sendrreply done and send->ack is %d \n",sentBuffer->ack);

		if ((sendPending == TRUE) && (sentBuffer == rReplyMsg || sentBuffer == rReplyMsg2)) {
			sendPending = FALSE;
			return SUCCESS;
		}
		else{
			return FAIL;
		}
    }
    
    event result_t SendRerr.sendDone(TOS_MsgPtr sentBuffer, bool success) {
		if ((sendPending == TRUE) && (sentBuffer == rErrMsg)) {
			sendPending = FALSE;
			return SUCCESS;
		}
		else{
			return FAIL;
		}
    }

	event result_t SendDadd.sendDone(TOS_MsgPtr sentBuffer, bool success) {
		if ((sendPending == TRUE) && (sentBuffer == daddMsg)) {
			sendPending = FALSE;
			return SUCCESS;
		}
		else{
			return FAIL;
		}
    }

	event result_t SendNadd.sendDone(TOS_MsgPtr sentBuffer, bool success) {
		if ((sendPending == TRUE) && (sentBuffer == naddMsg)) {
			sendPending = FALSE;
			return SUCCESS;
		}
		else{
			return FAIL;
		}
    }
	
	event result_t SendNadd_ack.sendDone(TOS_MsgPtr sentBuffer, bool success) {
		if ((sendPending == TRUE) && (sentBuffer == nadd_ackMsg)) {
			sendPending = FALSE;
			return SUCCESS;
		}
		else{
			return FAIL;
		}
    }
	
	
    //
    // Handle a AODV Rreq message
    //
	void ProcessRREQSSE(TOS_MsgPtr receivedMsg){
		SAODV_Rreq_Single_Signature_MsgPtr msgsingle;
		wsnAddr prevHop;
		uint32_t lon;
		uint32_t i;
		uint32_t message[30];
				
		call RreqPayload.linkPayload(receivedMsg, (uint8_t **) &msgsingle);
		dbg(DBG_USR1,"PROCESS RREQ-SSE\n");
		
		
#if 0
		    indx = getCacheIndex(msgsingle->src,msgsingle->dest);
		  
		    if(indx != INVALID_INDEX && msgsingle->metric[0] >= rreqCache[indx].numHops){
				// no need t update
				return ;
		    }
#endif	
		
		if(msgsingle->dest == TOS_LOCAL_ADDRESS){ 
		    //simplified AODV -- only destinations send route reply
		    if(msgsingle->destSeq > seq) {
				// not going to happen -- just a sanity check
				dbg(DBG_ROUTE, "ERROR:AODV_Core msg->destSeq:%d > seq:%d\n",seq,msgsingle->destSeq);
				return ;
		    }
	
			if(checkRREQSingle(msgsingle)==FALSE){
				dbg(DBG_USR1,"ERROR: The RREQ-SINGLE message had been modified\n");
				return ;
			}


		    if(rreplyTaskPending == TASK_DONE){
				seq++;   
				fwdRreply.type=AODV_RREP;
				fwdRreply.ra_reserved_and_prefix_size=0;
				fwdRreply.dest = msgsingle->dest;
				fwdRreply.src = msgsingle->src;
				fwdRreply.destSeq = seq;
				fwdRreply.metric[0] = 0; //destination is 0 hops away
				fwdRreply.lifetime=4000;
				rReplyDest = call SingleHopMsg.getSrcAddress(receivedMsg);
				rReplySrc = TOS_LOCAL_ADDRESS;
		
				fwdRreply.type_extension=SAODV_RREP_SINGLE_SIGNATURE_EXTENSION;
				fwdRreply.length=0;
				fwdRreply.hash_function=DEFAULT_HASH_FUNCTION;
				fwdRreply.maxHopCount=DEFAULT_MAX_HOP_COUNT;
						
				lon=SHA1HashSize/4;
			
				for(i=0;i<lon;i++){
					fwdRreply.hash[i]=0;
				}
			
				
			
				fwdRreply.hash[4]=call Random.rand();
				calculateHash2(fwdRreply.maxHopCount,fwdRreply.hash,5,fwdRreply.hash_function);
				arraychunkcpy(fwdRreply.topHash,res,SHA1HashSize/4,0);
			
				calculateHash2(0,fwdRreply.hash,5,fwdRreply.hash_function);
				arraychunkcpy(fwdRreply.hash,res,SHA1HashSize/4,0);
				
				fwdRreply.signMethod=DEFAULT_SIGN_METHOD;
				fwdRreply.H_and_Reserved=0;
				fwdRreply.paddLength=(uint8_t) (call Random.rand()%255);
				
				if(fwdRreply.paddLength %32 !=0){
					fwdRreply.paddLength=fwdRreply.paddLength+32;
				}
				
				fwdRreply.paddLength=fwdRreply.paddLength/32;
				for(i=0;i<fwdRreply.paddLength;i++){
					fwdRreply.padding[i]=0;
				}
				
				keycpy(fwdRreply.publicKey);
				
				message[0]=fwdRreply.type;
				message[0]=message[0]<<16 | fwdRreply.ra_reserved_and_prefix_size;
				message[0]=message[0]<<8 | 0;
								
				message[1]=fwdRreply.dest;
				message[2]=fwdRreply.destSeq;
				message[3]=fwdRreply.src;
				message[4]=fwdRreply.lifetime;
				message[5]=fwdRreply.type_extension;
				message[5]=message[5]<<16 | fwdRreply.hash_function;
				message[5]=message[5]<<8 | fwdRreply.maxHopCount;
					
				arraychunkcpy(message,fwdRreply.topHash,5,6);
				lon=5;
				message[6+lon]=fwdRreply.signMethod;
				message[6+lon]=message[6+lon]<<16 |fwdRreply.H_and_Reserved;
				message[6+lon]=message[6+lon]<<8 | fwdRreply.paddLength;
				
				arraychunkcpy(message,fwdRreply.publicKey,(fwdRreply.publicKey[0]&0x0ff)+1,7+lon);
				lon=lon+(fwdRreply.publicKey[0]&0x0ff)+1;
				arraychunkcpy(message,fwdRreply.padding,(fwdRreply.paddLength/32),7+lon);
				lon=lon+(fwdRreply.paddLength/32);
				signMessage(7+lon,message,fwdRreply.publicKey,fwdRreply.hash_function,fwdRreply.signature);

				
			
			
			
				if(updateCache(msgsingle->dest,msgsingle->src,msgsingle->rreqID,msgsingle->metric[0],msgsingle->destSeq,msgsingle->publicKey,rReplyDest)) {
					rreplyTaskPending = TASK_PENDING;
					//msg = *fwdRreply;
					post forwardRreplySingle();      
				}
		    }
		
		
		}
		else{
			
			if(checkRREQSingle(msgsingle)==FALSE){
				dbg(DBG_USR1,"ERROR: The RREQ-SINGLE message had been modified, acuerdate de corregirlo\n");//borrar acuerdate de corregirlo
				return ;
			}
			
			if(rreqTaskPending == TASK_DONE){
		
				prevHop = call SingleHopMsg.getSrcAddress(receivedMsg);
					
				if(msgsingle->src != TOS_LOCAL_ADDRESS && updateCache(msgsingle->dest,msgsingle->src,msgsingle->rreqID,msgsingle->metric[0],msgsingle->destSeq,msgsingle->publicKey,prevHop)) {
					rreqTaskPending = TASK_PENDING;
					fwdRreq = *msgsingle;
					post forwardRreqSingle(); 
				}
			}
		
			
		
		}
			
		return ;
	}
	
	void signRREPDSE(){
		uint32_t lon;
		uint32_t message[50];
		message[0]=fwdRreplyDouble.type;
		message[0]=message[0]<<16 | fwdRreplyDouble.ra_reserved_and_prefix_size;
		message[0]=message[0]<<8 | 0;
										
		message[1]=fwdRreplyDouble.dest;
		message[2]=fwdRreplyDouble.destSeq;
		message[3]=fwdRreplyDouble.src;
		message[4]=fwdRreplyDouble.lifetime;
		message[5]=fwdRreplyDouble.type_extension;
		message[5]=message[5]<<16 | fwdRreplyDouble.hash_function;
		message[5]=message[5]<<8 | fwdRreplyDouble.maxHopCount;
						
		arraychunkcpy(message,fwdRreplyDouble.topHash,5,6);
		lon=5;
		message[6+lon]=fwdRreplyDouble.signMethod2;
		message[6+lon]=message[6+lon]<<16 |fwdRreplyDouble.H_and_Reserved /*fwdRreplyDouble.H_and_Reserved2 Uncomment when TinyOs supports msg_size>120*/;
		message[6+lon]=message[6+lon]<<8 | fwdRreplyDouble.paddLength2;
						
		arraychunkcpy(message,fwdRreplyDouble.publicKey2,(fwdRreplyDouble.publicKey2[0]&0x0ff)+1,7+lon);
		lon=lon+(fwdRreplyDouble.publicKey2[0]&0x0ff)+1;
			
		/*Uncomment when TinyOs supports msg_size above 120*/	
		//arraychunkcpy(message,fwdRreplyDouble.padding2,fwdRreplyDouble.paddLength2/32,7+lon);
		//lon=lon+(fwdRreplyDouble.paddLength2/32);
		arraychunkcpy(message,fwdRreplyDouble.padding,fwdRreplyDouble.paddLength2/32,7+lon);
		lon=lon+(fwdRreplyDouble.paddLength2/32);			
		
		signMessage(7+lon,message,fwdRreplyDouble.publicKey2,fwdRreplyDouble.hash_function,fwdRreplyDouble.signature2);
				
	}
	
	void signRREPDSE2(){
		uint32_t lon;
		
		uint32_t message[50];
		message[0]=fwdRreplyDouble.type;
		message[0]=message[0]<<16 | fwdRreplyDouble2.ra_reserved_and_prefix_size;
		message[0]=message[0]<<8 | 0;
		message[1]=fwdRreplyDouble2.dest;
		message[2]=fwdRreplyDouble2.destSeq;
		message[3]=fwdRreplyDouble2.src;
		message[4]=fwdRreplyDouble2.lifetime;
		message[5]=fwdRreplyDouble2.type_extension;
		message[5]=message[5]<<16 | fwdRreplyDouble2.hash_function;
		message[5]=message[5]<<8 | fwdRreplyDouble2.maxHopCount;
						
		arraychunkcpy(message,fwdRreplyDouble2.topHash,5,6);
		lon=5;
		message[6+lon]=fwdRreplyDouble2.signMethod2;
		message[6+lon]=message[6+lon]<<16 |fwdRreplyDouble2.H_and_Reserved /*fwdRreplyDouble.H_and_Reserved2 Uncomment when TinyOs supports msg_size>120*/;
		message[6+lon]=message[6+lon]<<8 | fwdRreplyDouble2.paddLength2;
						
		arraychunkcpy(message,fwdRreplyDouble2.publicKey2,(fwdRreplyDouble2.publicKey2[0]&0x0ff)+1,7+lon);
		lon=lon+(fwdRreplyDouble2.publicKey2[0]&0x0ff)+1;
			
		/*Uncomment when TinyOs supports msg_size above 120*/	
		//arraychunkcpy(message,fwdRreplyDouble.padding2,fwdRreplyDouble.paddLength2/32,7+lon);
		//lon=lon+(fwdRreplyDouble.paddLength2/32);
		arraychunkcpy(message,fwdRreplyDouble2.padding,fwdRreplyDouble2.paddLength2/32,7+lon);
		lon=lon+(fwdRreplyDouble2.paddLength2/32);			
		
		signMessage(7+lon,message,fwdRreplyDouble2.publicKey2,fwdRreplyDouble2.hash_function,fwdRreplyDouble2.signature2);

						
	}
	
	void ProcessRREQDSE(TOS_MsgPtr receivedMsg){
		SAODV_Rreq_Double_Signature_MsgPtr msgdouble;
		wsnAddr prevHop;
		uint32_t lon;
		uint32_t i;
		int entry;
		uint32_t message[50];
		call RreqPayload.linkPayload(receivedMsg, (uint8_t **) &msgdouble);
		dbg(DBG_USR1,"PROCESS RREQ-DSE\n");
		#if 0
		    indx = getCacheIndex(msgdouble->src,msgdouble->dest);
		  
		    if(indx != INVALID_INDEX && msgdouble->metric[0] >= rreqCache[indx].numHops){
				// no need t update
				return ;
		    }
#endif	
	
		if(msgdouble->dest == TOS_LOCAL_ADDRESS){ 
		    //simplified AODV -- only destinations send route reply
#if AODV_CORE_DEBUG
			dbg(DBG_USR1,"RREQ-DSE answer by destination\n");
#endif 
		
		    if(msgdouble->destSeq > seq) {
				// not going to happen -- just a sanity check
				dbg(DBG_ROUTE, "ERROR:AODV_Core msg->destSeq:%d > seq:%d\n",seq,msgdouble->destSeq);
				return ;
		    }
	
			if(checkRREQDouble(msgdouble)==FALSE){
				dbg(DBG_USR1,"ERROR: The RREQ-DOUBLE message had been modified\n");
				return ;
			}	


		   
		    if(rreplyTaskPending == TASK_DONE){
				seq++;   
				fwdRreply.type=AODV_RREP;
				fwdRreply.ra_reserved_and_prefix_size=0;
				fwdRreply.dest = msgdouble->dest;
				fwdRreply.src = msgdouble->src;
				fwdRreply.destSeq = seq;
				fwdRreply.metric[0] = 0; //destination is 0 hops away

				fwdRreply.lifetime=4000;
				rReplyDest = call SingleHopMsg.getSrcAddress(receivedMsg);
				rReplySrc = TOS_LOCAL_ADDRESS;
		
				fwdRreply.type_extension=SAODV_RREP_SINGLE_SIGNATURE_EXTENSION;
				fwdRreply.length=0;
				fwdRreply.hash_function=DEFAULT_HASH_FUNCTION;
				fwdRreply.maxHopCount=DEFAULT_MAX_HOP_COUNT;
						
				lon=SHA1HashSize/4;
			
				for(i=0;i<lon;i++){
					fwdRreply.hash[i]=0;
				}
			
				
			
				fwdRreply.hash[4]=call Random.rand();
				calculateHash2(fwdRreply.maxHopCount,fwdRreply.hash,5,fwdRreply.hash_function);
				arraychunkcpy(fwdRreply.topHash,res,SHA1HashSize/4,0);
				calculateHash2(0,fwdRreply.hash,5,fwdRreply.hash_function);
				arraychunkcpy(fwdRreply.hash,res,SHA1HashSize/4,0);
			
				fwdRreply.signMethod=DEFAULT_SIGN_METHOD;
				fwdRreply.H_and_Reserved=0;
				fwdRreply.paddLength=1;//(uint8_t) (call Random.rand()%255);
				
				if(fwdRreply.paddLength %32 !=0){
					fwdRreply.paddLength=fwdRreply.paddLength+32;
				}
				
				fwdRreply.paddLength=fwdRreply.paddLength/32;
				for(i=0;i<fwdRreply.paddLength;i++){
					fwdRreply.padding[i]=0;
				}
				
				keycpy(fwdRreply.publicKey);
				
				
				message[0]=fwdRreply.type;
				
				message[0]=message[0]<<16 | fwdRreply.ra_reserved_and_prefix_size;
				message[0]=message[0]<<8 | 0;
								
				message[1]=fwdRreply.dest;
				message[2]=fwdRreply.destSeq;
				message[3]=fwdRreply.src;
				message[4]=fwdRreply.lifetime;
	
				message[5]=fwdRreply.type_extension;
				message[5]=message[5]<<16 | fwdRreply.hash_function;
				message[5]=message[5]<<8 | fwdRreply.maxHopCount;
				
				arraychunkcpy(message,fwdRreply.topHash,5,6);
				lon=5;
				
				message[6+lon]=fwdRreply.signMethod;
				message[6+lon]=message[6+lon]<<16 |fwdRreply.H_and_Reserved;
				message[6+lon]=message[6+lon]<<8 | fwdRreply.paddLength;
				
				arraychunkcpy(message,fwdRreply.publicKey,(fwdRreply.publicKey[0]&0x0ff)+1,7+lon);
				lon=lon+(fwdRreply.publicKey[0]&0x0ff)+1;
				arraychunkcpy(message,fwdRreply.padding,(fwdRreply.paddLength/32),7+lon);
				lon=lon+(fwdRreply.paddLength/32);
				signMessage(7+lon,message,fwdRreply.publicKey,fwdRreply.hash_function,fwdRreply.signature);
	
					
					
					
				if(updateCache(msgdouble->dest,msgdouble->src,msgdouble->rreqID,msgdouble->metric[0],msgdouble->destSeq,msgdouble->publicKey,rReplyDest)) {
					rreplyTaskPending = TASK_PENDING;
					post forwardRreplySingle();      
				}
		    }
			
		}
		
		else{
			if(msgdouble->jrgdu_and_reserved ==D_FLAG){
		
				if(checkRREQDouble(msgdouble)==FALSE){
					dbg(DBG_USR1,"ERROR: The RREQ-DOUBLE message had been modified\n");
					return ;
				}	
				
				if(rreqTaskPending == TASK_DONE){
					prevHop = call SingleHopMsg.getSrcAddress(receivedMsg);
			
					if(msgdouble->src != TOS_LOCAL_ADDRESS && updateCache(msgdouble->dest,msgdouble->src,msgdouble->rreqID,msgdouble->metric[0],msgdouble->destSeq,msgdouble->publicKey,prevHop)) {
					    rreqTaskPending = TASK_PENDING;
					    fwdRreqDouble = *msgdouble;
					    post forwardRreqDouble(); 
					}
				}
								
			}
			else{
				
					entry=getRTableIndex(msgdouble->dest);
					
					if(entry!=INVALID_INDEX){
#if AODV_CORE_DEBUG      
						dbg(DBG_USR1,"ReceiveRreq: RREQ answer by intermediate\n");
#endif
						seq++;    
						fwdRreplyDouble.type=AODV_RREP;
						fwdRreplyDouble.ra_reserved_and_prefix_size=0;
						fwdRreplyDouble.dest = msgdouble->src;
						fwdRreplyDouble.src = msgdouble->dest;
						fwdRreplyDouble.destSeq = msgdouble->destSeq;
						fwdRreplyDouble.metric[0] = msgdouble->metric[0]; //destination is 0 hops away
						fwdRreplyDouble.lifetime=4000;

						fwdRreplyDouble.type_extension=SAODV_RREP_DOUBLE_SIGNATURE_EXTENSION;
						fwdRreplyDouble.length=0;
						fwdRreplyDouble.hash_function=msgdouble->hash_function;
						fwdRreplyDouble.maxHopCount=msgdouble->maxHopCount;
						arraychunkcpy(fwdRreplyDouble.topHash,msgdouble->topHash,SHA1HashSize/4,0);

						arraychunkcpy(fwdRreplyDouble.hash,msgdouble->hash,SHA1HashSize/4,0);
									
						calculateHash2(0,fwdRreplyDouble.hash,5,fwdRreplyDouble.hash_function);
						arraychunkcpy(fwdRreplyDouble.hash,res,SHA1HashSize/4,0);
												
						fwdRreplyDouble.signMethod=msgdouble->signMethod;
						fwdRreplyDouble.H_and_Reserved=0;
						fwdRreplyDouble.paddLength=msgdouble->paddLength;

						//Uncomment when TinyOS support message size above 120
						for(i=0;i<fwdRreplyDouble.paddLength;i++){
							fwdRreplyDouble.padding[i]=0;
						}
					
						arraychunkcpy(fwdRreplyDouble.publicKey,msgdouble->publicKey,(msgdouble->publicKey[0]&0x0ff)+1,0);
						arraychunkcpy(fwdRreplyDouble.signature,msgdouble->signatureRREP,(msgdouble->signatureRREP[0]&0x0ff)+1,0);
						//Uncomment when TinyOs supports MSG_SIZE above 4000
						//fwdRreplyDouble.oldLifetime=4000;						
						fwdRreplyDouble.oldOriginatorIPAddress=msgdouble->src;
											
						fwdRreplyDouble.signMethod2=DEFAULT_SIGN_METHOD;
						
						fwdRreplyDouble.paddLength2=msgdouble->paddLength;
					//Uncomment when TinyOS support message size above 120	
					/*	fwdRreplyDouble.H_and_Reserved2=0;
						for(i=0;i<fwdRreplyDouble.paddLength2;i++){
							fwdRreplyDouble.padding2[i]=0;
						}
					*/	
						keycpy(fwdRreplyDouble.publicKey2);
						
						signRREPDSE();
												
						
						if(updateCache(msgdouble->dest,msgdouble->src,msgdouble->rreqID,msgdouble->metric[0],msgdouble->destSeq,msgdouble->publicKey,rReplyDest)) {
							rreplyTaskPending = TASK_PENDING;
							post forwardRreplyDouble();      
						}
						
						seq++;    
						fwdRreplyDouble2.type=AODV_RREP;
						fwdRreplyDouble2.ra_reserved_and_prefix_size=0;
						fwdRreplyDouble2.dest = msgdouble->dest;
						fwdRreplyDouble2.src = msgdouble->src;
						fwdRreplyDouble2.destSeq = routeTable[entry].destSeq;
						fwdRreplyDouble2.metric[0] = routeTable[entry].numHops; 
						fwdRreplyDouble2.lifetime=4000;
						rReplyDest = call SingleHopMsg.getSrcAddress(receivedMsg);
						rReplySrc = TOS_LOCAL_ADDRESS;
				
						fwdRreplyDouble2.type_extension=SAODV_RREP_DOUBLE_SIGNATURE_EXTENSION;
						fwdRreplyDouble2.length=0;
						fwdRreplyDouble2.hash_function=DEFAULT_HASH_FUNCTION;
						fwdRreplyDouble2.maxHopCount=DEFAULT_MAX_HOP_COUNT;
								
						lon=SHA1HashSize/4;
					
						arraychunkcpy(fwdRreplyDouble2.hash,routeTable[entry].hash,SHA1HashSize/4,0);
						arraychunkcpy(fwdRreplyDouble2.topHash,routeTable[entry].topHash,SHA1HashSize/4,0);
					
						fwdRreplyDouble2.signMethod=routeTable[entry].signMethod;
						fwdRreplyDouble2.H_and_Reserved=routeTable[entry].h_and_reserved;
						fwdRreplyDouble2.paddLength=routeTable[entry].paddLength;
						//Uncomment when TinyOS support message size above 120
						/*for(i=0;i<fwdRreplyDouble.paddLength;i++){
							fwdRreplyDouble.padding[i]=0;
						}
						*/
						arraychunkcpy(fwdRreplyDouble2.publicKey,routeTable[entry].publicKey,(routeTable[entry].publicKey[0]&0x0ff)+1,0);
						arraychunkcpy(fwdRreplyDouble2.signature,routeTable[entry].signature,(routeTable[entry].signature[0]&0x0ff)+1,0);
						
						fwdRreplyDouble2.oldOriginatorIPAddress=routeTable[entry].RREP_originator;
						//Uncomment when TinyOs support MSG_size above 120
						/*fwdRreplyDouble.oldLifetime=routeTable[entry].lifetime;
						*/
						fwdRreplyDouble2.signMethod2=DEFAULT_SIGN_METHOD;
						/*fwdRreplyDouble.H_and_Reserved2=0;
						fwdRreplyDouble.paddLength2=fwdRreplyDouble.paddLength;
						
						if(fwdRreplyDouble.paddLength2 %32 !=0){
							fwdRreplyDouble.paddLength2=fwdRreplyDouble.paddLength2+32;
						}
						
						fwdRreplyDouble.paddLength2=fwdRreplyDouble.paddLength2/32;*/
						fwdRreplyDouble2.paddLength2=fwdRreplyDouble.paddLength/32;
						//Uncomment when TinyOS support message size above 120
						/*for(i=0;i<fwdRreplyDouble.paddLength2;i++){
							fwdRreplyDouble.padding2[i]=0;
						}
						*/
						keycpy(fwdRreplyDouble2.publicKey2);
						
						signRREPDSE2();
					
						rreplyTaskPending2 = TASK_PENDING;
						post forwardRreplyDouble2(); 

						
						
						}
						
					else{
							
						if(rreqTaskPending == TASK_DONE){
							prevHop = call SingleHopMsg.getSrcAddress(receivedMsg);
							if(msgdouble->src != TOS_LOCAL_ADDRESS && updateCache(msgdouble->dest,msgdouble->src,msgdouble->rreqID,msgdouble->metric[0],msgdouble->destSeq,msgdouble->publicKey,prevHop)) {
							    rreqTaskPending = TASK_PENDING;
							    fwdRreqDouble = *msgdouble;
							    post forwardRreqDouble(); 
							}
						}
												
					}
		
			}
	
	
		}
	

		return;
		
		}
	
	event TOS_MsgPtr ReceiveRreq.receive(TOS_MsgPtr receivedMsg) {
		SAODV_Rreq_MsgPtr msg;
		
		call RreqPayload.linkPayload(receivedMsg, (uint8_t **) &msg);
		if((msg->metric[0] > AODV_MAX_METRIC) ||  (msg->metric[0] > msg->maxHopCount)){
			dbg(DBG_USR3,"Wrong: Max_Metric or Max_Hopcount exceeded \n");
			return receivedMsg;
		}
				
		if(!checkCache(msg->src,msg->rreqID,msg->metric[0],msg->publicKey)){
		    // message stale
			dbg(DBG_ROUTE, ("AODV_Core: checkcache fail\n"));
		    return receivedMsg;
		}
				
		//test sakm de sendDadd
		//dbg(DBG_USR1,"Test of sendDadd only for debugging\n");
		//daddSrc=msg->src;
		//post sendDadd();
			
		if( msg->src==TOS_LOCAL_ADDRESS){
			dbg(DBG_USR3,"Local address equals to destiny\n");
			return receivedMsg;
		}
				
		if(msg->type_extension==SAODV_RREQ_SINGLE_SIGNATURE_EXTENSION){
			atomic{ProcessRREQSSE(receivedMsg);}
		}
		else{
			atomic{ProcessRREQDSE(receivedMsg);}
		}
		
		return receivedMsg;
	}
	
	void ProcessRREPSSE(TOS_MsgPtr receivedMsg){
		
		SAODV_Rreply_MsgPtr msg;
		SAODV_Rreply_Single_Signature_MsgPtr msgsingle;
		
		dbg(DBG_ROUTE, "AODV_Core Rreply-SSE \n");
		dbg(DBG_ROUTE," mote emisor: %d\n",call SingleHopMsg.getSrcAddress(receivedMsg));
		call RreplyPayload.linkPayload(receivedMsg, (uint8_t **) &msgsingle);
		dbg(DBG_ROUTE, "AODV_Core Rreply.receive src: %d, dest: %d \n", msgsingle->dest, msgsingle->src);
		
		if(checkRREPSingle(msgsingle)==FALSE){
			dbg(DBG_USR1,"ERROR: The RREP-SSE message had been modified\n");
			return ;
		}
		
		
		if(msgsingle->src == TOS_LOCAL_ADDRESS){
		
			
		
						
			// notify components that need to be notified
#if AODV_CORE_DEBUG
			printRTable();
#endif
			rReplySrc = call SingleHopMsg.getSrcAddress(receivedMsg);
			addRtableEntry(msgsingle->dest,msgsingle->src,msgsingle->destSeq,msgsingle->metric[0],msgsingle->lifetime,msgsingle->publicKey,msgsingle->signature,msgsingle->hash,msgsingle->topHash,msgsingle->hash_function,msgsingle->paddLength,msgsingle->H_and_Reserved,msgsingle->signMethod);
			
			
			
#if AODV_CORE_DEBUG
			printRTable();
#endif
		}
		else{
			call RreplyPayload.linkPayload(receivedMsg, (uint8_t **) &msg);
			if((rReplyDest = getReverseRoute(msg)) != INVALID_NODE_ID && rreplyTaskPending == TASK_DONE){
				dbg(DBG_ROUTE, "AODV_Core GOING to call fwdRrreply\n");
				rreplyTaskPending = TASK_PENDING;
				fwdRreply = *msgsingle;
				/*puestos en forwardRReply fwdRreply.metric[0]++; */
				rReplySrc = call SingleHopMsg.getSrcAddress(receivedMsg);
				post forwardRreplySingle();      
			}	
		}
	
	}
	
	void ProcessRREPDSE(TOS_MsgPtr receivedMsg){
		
		SAODV_Rreply_MsgPtr msg;
		SAODV_Rreply_Double_Signature_MsgPtr msgdouble;

			
		

		call RreplyPayload.linkPayload(receivedMsg, (uint8_t **) &msgdouble);
#if AODV_CORE_DEBUG
		dbg(DBG_ROUTE, "AODV_Core Received Rreply-DSE \n");
		dbg(DBG_ROUTE," mote emisor: %d\n",call SingleHopMsg.getSrcAddress(receivedMsg));
		dbg(DBG_ROUTE, "AODV_Core Rreply.receive src: %d dest: %d \n", msgdouble->dest, msgdouble->src);
#endif
		
		if(checkRREPDouble(msgdouble)==FALSE){
		}
		
				
		if(msgdouble->src == TOS_LOCAL_ADDRESS){
						
					
			// notify components that need to be notified
#if AODV_CORE_DEBUG
			printRTable();
#endif
			rReplySrc = call SingleHopMsg.getSrcAddress(receivedMsg);
			addRtableEntry(msgdouble->dest,msgdouble->src,msgdouble->destSeq,msgdouble->metric[0],msgdouble->lifetime,msgdouble->publicKey,msgdouble->signature,msgdouble->hash,msgdouble->topHash,msgdouble->hash_function,msgdouble->paddLength,msgdouble->H_and_Reserved,msgdouble->signMethod);
					
#if AODV_CORE_DEBUG
			printRTable();
#endif
		}
		else{
			call RreplyPayload.linkPayload(receivedMsg, (uint8_t **) &msg);
			if((rReplyDest = getReverseRoute(msg)) != INVALID_NODE_ID && rreplyTaskPending == TASK_DONE){
#if AODV_CORE_DEBUG
				dbg(DBG_ROUTE, "AODV_Core GOING to call fwdRrreply\n");
#endif
				rreplyTaskPending = TASK_PENDING;
				fwdRreplyDouble = *msgdouble;
				rReplySrc = call SingleHopMsg.getSrcAddress(receivedMsg);
				// routing entry added in the forward reply
				post forwardRreplyDouble();   
				
		    }
		}

			
	}
	
	event TOS_MsgPtr ReceiveRreply.receive(TOS_MsgPtr receivedMsg) {

		SAODV_Rreply_MsgPtr msg;
			   
		call RreplyPayload.linkPayload(receivedMsg, (uint8_t **) &msg);
		if((msg->metric[0] > AODV_MAX_METRIC) ||  (msg->metric[0] > msg->maxHopCount)){
			dbg(DBG_USR3,"Wrong:Max_Metric or Max_Hopcount exceeded \n");
			return receivedMsg;
		}	

	
		if(msg->type_extension==SAODV_RREP_SINGLE_SIGNATURE_EXTENSION){
			atomic{ProcessRREPSSE(receivedMsg);}
		}else{
			atomic{ProcessRREPDSE(receivedMsg);}
		}

		return receivedMsg;
	}
			
	event TOS_MsgPtr ReceiveRerr.receive(TOS_MsgPtr receivedMsg) {

		SAODV_Rerr_Signature_Extension_MsgPtr msg;
		
		call RerrPayload.linkPayload(receivedMsg, (uint8_t **) &msg);
        dbg(DBG_ROUTE, ("SAODV_Core Rerr.receive \n"));
		rErrDest = msg->dest;
		rErrSrc = call SingleHopMsg.getSrcAddress(receivedMsg);
		
		if(checkRERR(msg)==FALSE){
			return receivedMsg;
		}

		
		if(rerrTaskPending == TASK_DONE){
			rerrTaskPending = TASK_PENDING;
			post fwdRerr();
		}
      
		return receivedMsg;
    }

	event TOS_MsgPtr ReceiveDadd.receive(TOS_MsgPtr receivedMsg){
		SAKM_Dadd_MsgPtr msg;
		uint32_t aux[2];
		dbg(DBG_ROUTE,("SAKM DADD  received \n"));
		
		call DaddPayload.linkPayload(receivedMsg,(uint8_t **) &msg);
		daddSrc=call SingleHopMsg.getSrcAddress(receivedMsg); //who is the sender of the message;
		
		keycpy(aux);
		if((msg->duplicated_node_ip_address[0] == TOS_LOCAL_ADDRESS) && (compareSignatures(msg->duplicated_node_public_key,aux)==FALSE)){
			post sendNadd();
		}
		else{
			position= checkRreqCache(msg->duplicated_node_ip_address[0],msg->duplicated_node_public_key);
			daddTaskPending=TASK_PENDING;
			post fwdDadd();
			
		}
		return receivedMsg;
	}

	event TOS_MsgPtr ReceiveNadd.receive(TOS_MsgPtr receivedMsg) {

		SAKM_Nadd_MsgPtr msg;
	
		call NaddPayload.linkPayload(receivedMsg, (uint8_t **) &msg);
        dbg(DBG_ROUTE, ("SAKM Nadd.receive \n"));
	
		if(checkNadd(msg)==FALSE){
			return receivedMsg;
		}
		
	
		if(markEntry(msg->old_ip,msg->oldpublicKey)==FAIL){
			return receivedMsg;
		}
				
		if(naddTaskPending == TASK_DONE /*&&  updateCache(msg->dest,msg->src,msg->rreqID,msg->metric[0],msg->destSeq,naddDest)*/){
			naddTaskPending = TASK_PENDING;
			forwardNadd= *msg;
			post fwdNadd();
		}
     
		if(nadd_ackTaskPending == TASK_DONE){
		   	nadd_ackTaskPending = TASK_PENDING;
			nadd_ackDest=1;//test sakm call SingleHopMsg.getSrcAddress(receivedMsg);
			post sendNadd_ack();
		
		}
		
		
		
		return receivedMsg;
    }

	event TOS_MsgPtr ReceiveNadd_ack.receive(TOS_MsgPtr receivedMsg) {

		SAKM_Nadd_ack_MsgPtr msg;
		
		call Nadd_ackPayload.linkPayload(receivedMsg, (uint8_t **) &msg);
        dbg(DBG_ROUTE, ("SAKM Nadd_ack.receive \n"));
	
		
		if(checkNadd_ack(msg)==FALSE){
		    return receivedMsg;
		}
		
      
		return receivedMsg;
}
	
    event result_t Timer.fired() {
		dbg(DBG_TEMP, ("AODV_Core Timer.fired()\n"));
        if(rreqTaskPending == TASK_REPOSTREQ){
			rreqTaskPending = TASK_PENDING;
			post resendRreq();
			return SUCCESS;
		}

        if(rreplyTaskPending == TASK_REPOSTREQ){
			rreplyTaskPending = TASK_PENDING;
	        post resendRreply();
			return SUCCESS;
        } 
		
		if(rreplyTaskPending2 == TASK_REPOSTREQ){
			rreplyTaskPending2 = TASK_PENDING;
	        post resendRreply();
			return SUCCESS;
        }
		
		
        if(rerrTaskPending == TASK_REPOSTREQ){
			rerrTaskPending = TASK_PENDING;
			post resendRerr();
			return SUCCESS;
        }

		if(daddTaskPending == TASK_REPOSTREQ){
			daddTaskPending = TASK_PENDING;
			post resendDadd();
			return SUCCESS;
        }
		
		if(naddTaskPending == TASK_REPOSTREQ){
			naddTaskPending = TASK_PENDING;
			post resendNadd();
			return SUCCESS;
        }
		
		if(nadd_ackTaskPending == TASK_REPOSTREQ){
			nadd_ackTaskPending = TASK_PENDING;
			post resendNadd_ack();
			return SUCCESS;
        }
		
      return SUCCESS;
    }    
    

    command wsnAddr RouteLookup.getNextHop(TOS_MsgPtr m, wsnAddr dest){
		int i;
		for(i =0; i <AODV_RTABLE_SIZE; i++){
		    if(routeTable[i].dest == dest){
				return routeTable[i].nextHop;
		    }
		}
		return INVALID_NODE_ID;
    }


    command wsnAddr RouteLookup.getRoot(){
		return AODV_ROOT_NODE;
    }
	
    command wsnAddr ReactiveRouter.getNextHop(wsnAddr dest){
		int i;
		for(i =0; i <AODV_RTABLE_SIZE; i++){
			if(routeTable[i].dest == dest){
				return routeTable[i].nextHop;
			}
		}
		return INVALID_NODE_ID;
    }	


    command result_t ReactiveRouter.generateRoute(wsnAddr dest){
		dbg(DBG_ROUTE, ("AODV_Core Received a request to generate route\n"));
		if(dest == TOS_LOCAL_ADDRESS){
			return FAIL;
		}
		if(rreqTaskPending == TASK_DONE){
			rreqTaskPending = TASK_PENDING;
			rreqDest = dest;
			post sendRreq();
			return SUCCESS;
		}
		else {
			return FAIL;
		}
    }
    
    command result_t RouteError.SendRouteErr(wsnAddr dest){
		dbg(DBG_ROUTE, ("AODV_Core Received a request to Send Route Error\n"));
		if(rerrTaskPending == TASK_DONE){
			rerrTaskPending = TASK_PENDING;
			rErrDest = dest;
			post sendRerr();
			return SUCCESS;
		}
		return FAIL;
    }
    
}

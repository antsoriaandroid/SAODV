typedef struct {
  wsnAddr   dest;
  wsnAddr	src;
  wsnAddr   nextHop;
  uint16_t  destSeq;
  uint8_t   numHops;
  uint16_t  rreqID;
  wsnAddr RREP_originator;
  wsnAddr RREQ_destination;
  uint32_t ra_and_reserved;
  uint8_t prefizSx;
  //uint8_t flags;
  uint32_t paddLength;
  uint32_t lifetime;
  uint8_t signMethod;
  uint8_t h_and_reserved;
  uint8_t hash_function;
  uint32_t hash[5];
  uint32_t topHash[5];
  uint32_t signature[2];
  uint32_t publicKey[2];
  
} AODV_Route_Table;


typedef struct {
  wsnAddr   dest;
  wsnAddr   src;
  wsnAddr   nextHop;
  uint16_t  rreqID;
  uint16_t  destSeq;
  uint8_t   numHops;
  wsnAddr RREP_originator;
  wsnAddr RREQ_destination;
  uint32_t ra_and_reserved;
  uint8_t prefizSx;
  //uint8_t flags;
  uint32_t lifetime;
  uint32_t paddLength;
  uint32_t old_lifetime;
    uint32_t hash[5];
  uint32_t topHash[5];
  uint32_t signature[2];
  uint32_t publicKey[2];
  uint8_t send;
} AODV_Route_Cache;



#define AODV_ROOT_NODE 0

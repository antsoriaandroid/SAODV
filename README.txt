TINY_SAODV 20090127

- Before doing anything we should modify TOSH_DATA_LENGTH to set it to 118 (TOSH_DATA_LENGTH is defined in the file 'AM.h' which is in '\opt\tinyos-1.x\tos\types').

- Some application files to test the protocol are in 'apps/TraceRouteTestAODV/' and are called 'TraceRouteTest' y 'TraceRouteTestM'.

- SAODV & SAKM protocol implementation source code is in '/PFC/tos/lib/'

1-INSTALLATION
- To install the program go first to 'apps/TraceRouteTestAODV/', then:

1.1-INSTALLING DEVICES
(Compiling the program and uploading the program in the devices)
- To install the program in the base station type:

make SINK_NODE=1 <platform> install.0 mib520, com1

where:
- '<platform>' is the sensor type (mica,micaZ,etc).
- 'mib520' refers to the base station connected to the PC (change if it's another one).
- 'com1' refers to the port of the base station.

- To install the program in the devices type:

make <platform> install.x mib520, com1

where:
- 'x' is the device id.

1.2-INSTALLATION IN THE SIMULATOR
(In this case only the executable file used by the simulator is generated)
- Just type:

make pc

2-RUNNING

2.1-IN THE SIMULATOR
Type:

./build/pc/main.exe num_nodes

(where 'num_nodes' refers to the number of nodes that we want to simulate)

2.2-IN THE DEVICES
Just switch them on

